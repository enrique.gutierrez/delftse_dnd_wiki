[[Animal Friendship]]
[[Animal Messenger]]
[[Animal Shapes]]
[[Antilife Shell]]
[[AntipathySympathy]]
[[Awaken]]
[[Barkskin]]
[[Blight]]
[[Call Lightning]]
[[Charm Person]]
[[Commune with Nature]]
[[Confusion]]
[[Conjure Animals]]
[[Conjure Elemental]]
[[Conjure Fey]]
[[Conjure Minor Elementals]]
[[Conjure Woodland Beings]]
[[Contagion]]
[[Control Water]]
[[Control Weather]]
[[Create or Destroy Water]]
[[Cure Wounds]]
[[Darkvision]]
[[Daylight]]
[[Detect Magic]]
[[Detect Poison and Disease]]
[[Dispel Magic]]
[[Dominate Beast]]
[[Druidcraft]]
[[Earthquake]]
[[Enhance Ability]]
[[Entangle]]
[[Faerie Fire]]
[[Feeblemind]]
[[Find the Path]]
[[Find Traps]]
[[Fire Storm]]
[[Flame Blade]]
[[Flaming Sphere]]
[[Fog Cloud]]
[[Foresight]]
[[Freedom of Movement]]
[[Geas]]
[[Giant Insect]]
[[Goodberry]]
[[Greater Restoration]]
[[Guidance]]
[[Gust of Wind]]
[[Hallucinatory Terrain]]
[[Heal]]
[[Healing Word]]
[[Heat Metal]]
[[Heroes' Feast]]
[[Hold Person]]
[[Ice Storm]]
[[Insect Plague]]
[[Jump]]
[[Lesser Restoration]]
[[Locate Animals or Plants]]
[[Locate Creature]]
[[Locate Object]]
[[Longstrider]]
[[Mass Cure Wounds]]
[[Meld into Stone]]
[[Mending]]
[[Mirage Arcane]]
[[Moonbeam]]
[[Move Earth]]
[[Pass without Trace]]
[[Planar Binding]]
[[Plane Shift]]
[[Plant Growth]]
[[Poison Spray]]
[[Polymorph]]
[[Produce Flame]]
[[Protection from Energy]]
[[Protection from Poison]]
[[Purify Food and Drink]]
[[Regenerate]]
[[Reincarnate]]
[[Resistance]]
[[Reverse Gravity]]
[[Scrying]]
[[Shapechange]]
[[Shillelagh]]
[[Sleet Storm]]
[[Speak with Animals]]
[[Speak with Plants]]
[[Spike Growth]]
[[Stone Shape]]
[[Stoneskin]]
[[Storm of Vengeance]]
[[Sunbeam]]
[[Sunburst]]
[[Thunderwave]]
[[Transport via Plants]]
[[Tree Stride]]
[[True Resurrection]]
[[Wall of Fire]]
[[Wall of Stone]]
[[Wall of Thorns]]
[[Water Breathing]]
[[Water Walk]]
[[Wind Walk]]
[[Wind Wall]]
# Cleric

## Spell List

```dataview
LIST
FROM
    #class/cleric 
    AND 
    "srd/Spells"
SORT file.name ASC
```
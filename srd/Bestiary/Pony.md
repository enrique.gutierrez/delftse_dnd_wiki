# Pony
>*Medium beast, unaligned*
>___
>- **Armor Class** 10
>- **Hit Points** 11 (2d8 + 2)
>- **Speed** 40 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|15 (+2)|10 (+0)|13 (+1)|2 (-4)|11 (+0)|7 (-2)|
>___
>- **Senses** passive Perception 10
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 #beast/cr/5 #beast/cr/6 #beast/cr/7 #beast/cr/8 (25 XP)
>## Actions
>***Hooves.*** Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) bludgeoning damage.
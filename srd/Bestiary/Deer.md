# Deer
>*Medium beast, unaligned*
>___
>- **Armor Class** 13
>- **Hit Points** 4 (1d8)
>- **Speed** 50 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|11 (+0)|16 (+3)|11 (+0)|2 (-4)|14 (+2)|5 (-3)|
>___
>- **Senses** passive Perception 12
>- **Languages** —
>- #beast/cr/0 (0 or 10 XP)
>## Actions
>***Bite.*** Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) piercing damage.
# Poisonous Snake
>*Tiny beast, unaligned*
>___
>- **Armor Class** 13
>- **Hit Points** 2 (1d4)
>- **Speed** 30 ft., swim 30 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|2 (-4)|16 (+3)|11 (+0)|1 (-5)|10 (+0)|3 (-4)|
>___
>- **Senses** blindsight 10 ft., passive Perception 10
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 #beast/cr/5 #beast/cr/6 #beast/cr/7 #beast/cr/8 (25 XP)
>## Actions
>***Bite.*** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage, and the target must make a DC 10 Constitution saving throw, taking 5 (2d4) poison damage on a failed save, or half as much damage on a successful one.
# Avatar of Death
>*Medium undead, neutral evil*
>___
>- **Armor Class** 20
>- **Hit Points** half the hit point maximum of its summoner
>- **Speed** 60 ft., fly 60 ft. (hover)
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|16 (+3)|16 (+3)|16 (+3)|16 (+3)|16 (+3)|16 (+3)|
>___
>- **Damage Immunities** necrotic, poison
>- **Condition Immunities** charmed, frightened, paralyzed, petrified, poisoned, unconscious
>- **Senses** darkvision 60 ft., truesight 60 ft., passive Perception 13
>- **Languages** all languages known to its summoner
>- #beast/cr/-—
>___
>***Incorporeal Movement.*** The avatar can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.  
>
>***Turn Immunity.*** The avatar is immune to features that turn undead.  
>
>### Actions
>***Reaping Scythe.*** The avatar sweeps its spectral scythe through a creature within 5 feet of it, dealing 7 (1d8 + 3) slashing damage plus 4 (1d8) necrotic damage.

## Avatar of Death

Summoned by the "Skull" card from the Deck of Many Things.
# Giant Rat
>*Small beast, unaligned*
>___
>- **Armor Class** 12
>- **Hit Points** 7 (2d6)
>- **Speed** 30 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|7 (-2)|15 (+2)|11 (+0)|2 (-4)|10 (+0)|4 (-3)|
>___
>- **Senses** darkvision 60 ft., passive Perception 10
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 #beast/cr/5 #beast/cr/6 #beast/cr/7 #beast/cr/8 (25 XP)
>___
>***Keen Smell.*** The rat has advantage on Wisdom (Perception) checks that rely on smell.  
>
>***Pack Tactics.*** The rat has advantage on an attack roll against a creature if at least one of the rat's allies is within 5 feet of the creature and the ally isn't incapacitated.  
>
>### Actions
>***Bite.*** Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
# Mule
>*Medium beast, unaligned*
>___
>- **Armor Class** 10
>- **Hit Points** 11 (2d8 + 2)
>- **Speed** 40 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|14 (+2)|10 (+0)|13 (+1)|2 (-4)|10 (+0)|5 (-3)|
>___
>- **Senses** passive Perception 10
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 #beast/cr/5 #beast/cr/6 #beast/cr/7 #beast/cr/8 (25 XP)
>___
>***Beast of Burden.*** The mule is considered to be a Large animal for the purpose of determining its carrying capacity.  
>
>***Sure-Footed.*** The mule has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.  
>
>### Actions
>***Hooves.*** Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.
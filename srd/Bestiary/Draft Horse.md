# Draft Horse
>*Large beast, unaligned*
>___
>- **Armor Class** 10
>- **Hit Points** 19 (3d10 + 3)
>- **Speed** 40 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|18 (+4)|10 (+0)|12 (+1)|2 (-4)|11 (+0)|7 (-2)|
>___
>- **Senses** passive Perception 10
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 (50 XP)
>## Actions
>***Hooves.*** Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) bludgeoning damage.
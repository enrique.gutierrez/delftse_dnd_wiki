# Giant Weasel
>*Medium beast, unaligned*
>___
>- **Armor Class** 13
>- **Hit Points** 9 (2d8)
>- **Speed** 40 ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|11 (+0)|16 (+3)|10 (+0)|4 (-3)|12 (+1)|5 (-3)|
>___
>- **Skills** Perception +3, Stealth +5
>- **Senses** darkvision 60 ft., passive Perception 13
>- **Languages** —
>- #beast/cr/1 #beast/cr/2 #beast/cr/3 #beast/cr/4 #beast/cr/5 #beast/cr/6 #beast/cr/7 #beast/cr/8 (25 XP)
>___
>***Keen Hearing and Smell.*** The weasel has advantage on Wisdom (Perception) checks that rely on hearing or smell.  
>
>### Actions
>***Bite.*** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
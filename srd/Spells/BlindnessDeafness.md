# Blindness/Deafness
*#spell/level/2nd #spell/type/necromancy*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V
- **Duration:** 1 minute
---
You can blind or deafen a foe. Choose one creature that you can see within range to make a Constitution saving throw. If it fails, the target is either [[Blinded]] or [[Deafened]] (your choice) for the duration. At the end of each of its turns, the target can make a Constitution saving throw. On a success, the spell ends.

***At Higher Levels.*** When you cast this spell using a spell slot of 3rd level or higher, you can target one additional creature for each slot level above 2nd.
#class/bard
#class/cleric
#class/sorcerer
#class/wizard
# Greater Invisibility
*#spell/level/4th #spell/type/illusion*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S
- **Duration:** Concentration, up to 1 minute
---
You or a creature you touch becomes [[Invisible]] until the spell ends. Anything the target is wearing or carrying is [[Invisible]] as long as it is on the target's person.
#class/bard
#class/sorcerer
#class/wizard
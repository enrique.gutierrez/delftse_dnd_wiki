# Hypnotic Pattern
*#spell/level/3rd #spell/type/illusion*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** S, M (a glowing stick of incense or a crystal vial filled with phosphorescent material)
- **Duration:** Concentration, up to 1 minute
---
You create a twisting pattern of colors that weaves through the air inside a 30-foot cube within range. The pattern appears for a moment and vanishes. Each creature in the area who sees the pattern must make a Wisdom saving throw. On a failed save, the creature becomes [[Charmed]] for the duration. While [[Charmed]] by this spell, the creature is [[Incapacitated]] and has a speed of 0.

The spell ends for an affected creature if it takes any damage or if someone else uses an action to shake the creature out of its stupor.
#class/bard
#class/sorcerer
#class/warlock
#class/wizard
# Protection from Poison
*#spell/level/2nd #spell/type/abjuration*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S
- **Duration:** 1 hour
---
You touch a creature. If it is [[Poisoned]], you neutralize the poison. If more than one poison afflicts the target, you neutralize one poison that you know is present, or you neutralize one at random.

For the duration, the target has advantage on saving throws against being [[Poisoned]], and it has resistance to poison damage.
#class/cleric
#class/druid
#class/paladin
#class/ranger
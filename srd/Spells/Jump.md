# Jump
*#spell/level/1st #spell/type/transmutation*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S, M (a grasshopper's hind leg)
- **Duration:** 1 minute
---
You touch a creature. The creature's *jump distance* is tripled until the spell ends.
#class/druid
#class/ranger
#class/sorcerer
#class/wizard
# Misty Step
*#spell/level/2nd #spell/type/conjuration*
___ 
#spell
- **Casting Time:** 1 bonus action
- **Range:** Self
- **Components:** V
- **Duration:** Instantaneous
---
Briefly surrounded by silvery mist, you teleport up to 30 feet to an unoccupied space that you can see.
#class/sorcerer
#class/warlock
#class/wizard
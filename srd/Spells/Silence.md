# Silence
*#spell/level/2nd #spell/type/illusion #spell/type/ritual*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S
- **Duration:** Concentration, up to 10 minutes
---
For the duration, no sound can be created within or pass through a 20-foot-radius sphere centered on a point you choose within range. Any creature or object entirely inside the sphere is immune to thunder damage, and creatures are [[Deafened]] while entirely inside it. Casting a spell that includes a verbal component is impossible there.
#class/bard
#class/cleric
#class/ranger
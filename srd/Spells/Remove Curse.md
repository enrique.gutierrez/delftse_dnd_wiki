# Remove Curse
*#spell/level/3rd #spell/type/abjuration*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S
- **Duration:** Instantaneous
---
At your touch, all curses affecting one creature or object end. If the object is a cursed magic item, its curse remains, but the spell breaks its owner's attunement to the object so it can be removed or discarded.
#class/cleric
#class/paladin
#class/warlock
#class/wizard
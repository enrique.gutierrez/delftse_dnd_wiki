# Protection from Energy
*#spell/level/3rd #spell/type/abjuration*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S
- **Duration:** Concentration, up to 1 hour
---
For the duration, the willing creature you touch has resistance to one damage type of your choice: acid, cold, fire, lightning, or thunder.
#class/cleric
#class/druid
#class/ranger
#class/sorcerer
#class/wizard
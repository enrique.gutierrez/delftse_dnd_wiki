# Divine Favor
*#spell/level/1st #spell/type/evocation*
___ 
#spell
- **Casting Time:** 1 bonus action
- **Range:** Self
- **Components:** V, S
- **Duration:** Concentration, up to 1 minute
---
Your prayer empowers you with divine radiance. Until the spell ends, your weapon attacks deal an extra 1d4 radiant damage on a hit.
#class/paladin
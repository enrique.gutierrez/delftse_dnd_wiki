# Lesser Restoration
*#spell/level/2nd #spell/type/abjuration*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S
- **Duration:** Instantaneous
---
You touch a creature and can end either one disease or one condition afflicting it. The condition can be [[Blinded]], [[Deafened]], [[Paralyzed]], or [[Poisoned]].
#class/bard
#class/cleric
#class/druid
#class/paladin
#class/ranger
# Power Word Stun
*#spell/level/8th #spell/type/enchantment*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** V
- **Duration:** Instantaneous
---
You speak a word of power that can overwhelm the mind of one creature you can see within range, leaving it dumbfounded. If the target has 150 hit points or fewer, it is [[Stunned]]. Otherwise, the spell has no effect.

The [[Stunned]] target must make a Constitution saving throw at the end of each of its turns. On a successful save, this stunning effect ends.
#class/bard
#class/sorcerer
#class/warlock
#class/wizard
# See Invisibility
*#spell/level/2nd #spell/type/divination*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** Self
- **Components:** V, S, M (a pinch of Talc and a small sprinkling of powdered silver)
- **Duration:** 1 hour
---
For the duration, you see [[Invisible]] creatures and objects as if they were visible, and you can see into the Ethereal Plane. Ethereal creatures and objects appear ghostly and translucent.
#class/bard
#class/sorcerer
#class/wizard
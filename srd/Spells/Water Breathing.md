# Water Breathing
*#spell/level/3rd #spell/type/transmutation #spell/type/ritual*
___ 
#spell
- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, M (a short reed or piece of straw)
- **Duration:** 24 hours
---
This spell grants up to ten willing creatures you can see within range the ability to breathe underwater until the spell ends. Affected creatures also retain their normal mode of respiration.
#class/druid
#class/ranger
#class/sorcerer
#class/wizard
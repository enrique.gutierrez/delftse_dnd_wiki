# Info

Repository for DND campaign

# Reading instructions

Multiple options:

- Most basic with your browser: Go to the [campaigns folder](https://gitlab.com/enrique.gutierrez/delftse_dnd_wiki/-/tree/main/campaings) and see the history.
- Using Obsidian: https://obsidian.md/
    - Simple option: 
        - Download the Zip to your computer 
            - ![](attachments/Pasted%20image%2020201025171954.png)
        - "Open folder as Vault"
            - ![](attachments/Pasted%20image%2020201025172435.png)
    - More complicated (requires `git` installed in your computer):
        -  Clone the repository
            - ![](attachments/Pasted%20image%2020201025172012.png)
        - "Open folder as Vault"
            - ![](attachments/Pasted%20image%2020201025172435.png)


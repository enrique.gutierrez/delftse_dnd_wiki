# Recap

- [Norman](../characters/Norman.md) stabbed [Sir Evan](../characters/Sir%20Evan.md)
    - [Vosha](../characters/Vosha.md) attacked [Norman](../characters/Norman.md)
    - [Lady Angharad](../characters/Lady%20Angharad.md) attacked [Norman](../characters/Norman.md) and [Vosha](../characters/Vosha.md)
    - [Elidee](../characters/Elidee.md) helped [Norman](../characters/Norman.md) escape in secret to the city
- [Xairan](../characters/Xairan.md) and [Lord Viridian](../characters/Lord%20Viridian.md) talked about the events that transpire trying to defuse further escalation.
- [Urist](../characters/Urist.md) in the city got several kg of cheese to give back to the cheese mafia.
    - He's returned to the [Safe house](../locations/City%20of%20the%20South.md#Safe%20house)


# Interrogation room

> Inside a room, [Vosha](../characters/Vosha.md), [Sir Evan](../characters/Sir%20Evan.md), and [Elidee](../characters/Elidee.md) and [Lord Samuel](../characters/Lord%20Samuel.md)
> [Amryn](../characters/Amryn.md) can overhear the room
> ![](../attachments/Pasted%20image%2020210110132518.png)

- Sir Evan asks Vosha: What was that all about?
    - Vosha: I should've done better. I wanted to make sure it would be safe and we couldn't trust them.
    - Sir Evan: Why them?
    - Vosha: They trust the coin and have previously worked aligned.
    - Sir Evan: at least the rest of the party were good to me. I **would like to meet the man who stabbed me** The rest of the party didin't seem to kind to you. Your acts of violence have been noted for the castle. You need to apologize, with the governor. There must be a reason for him to have stabbed me, are you only an acquaintance?
        - Vosha: Yes, he was only a casual acquaintance (*lie*)
    - Lord Samuel: How do we know we can trust you if your father was a traitor
        - Sir Evan: We 

> Elidee is blending into the background and stay outside of the conversation

> Amryn stays hidden.

# Norman on the run

> [Norman](../characters/Norman.md) after having escaped - helped by [Elidee](../characters/Elidee.md) - is hiding via [[Disguise Self]].

- Norman #cast [[Healing Word]]
- Norman is reaching the [Safe house](../locations/City%20of%20the%20South.md#Safe%20house)

> [Urist](../characters/Urist.md) is at the home.

- Norman drops [[Disguise Self]]
    - Urist: Oh, there you are!!
    - Norman: How's the party?
    - Urist: It was a big mess
    - Norman: Well, they didn't die. I didn't finish the job.
    - Urist: Sir Evan?
        - Norman: Yes. **He killed my mother and sister**. They were casualties of the lord's abuse.
- Urist: Well, the problem is that we're surrounded in the city. When you stabbed him you've technically attacked the governor. He's mad  as hell
    - Norman: It was a shitty ball. It needed some action
    - Urist: He's going to have hit out for you.
    - Norman: I like the challange.
    - Urist: How good are you *standing on stilts* ?
        - Norman: As long as I don't have an arrow on my chest.
        - Urist: We could disguise you as a human if you're taller.

> DM (Narrator): Given the backstory of Norman, it **would be possible** for him to walk on stilts.

- Urist: I think we should hide you soon. If they're looking for you they'll come here soon. Maybe we should **put you in a barrel**.
    - Norman: Sounds good.
    - Urist: [[Checks#Investigation]] 16
        - Walking on the streets, Urists finds a barrel

# Returning to the ball room
> The [Interrogation room](Session_210110.md#Interrogation%20room) scene ends, and the group exists the room to go back to the ball room

- [Amryn](../characters/Amryn.md) is going to hide as she notices people leaving the room
    - Amryn [[Checks#Stealth]] 25 to hide from the group

> The group walks out and goes up the stairs and back, trailed stealthily by Amryn.

- [Sir Evan](../characters/Sir%20Evan.md): I would like my squire to apologize to you for
    - [Lord Viridian](../characters/Lord%20Viridian.md): Yes, I've been conviced that it should be handled privately. Looking at [Xairan](../characters/Xairan.md)
    - [Vosha](../characters/Vosha.md): Yes, this was my fault.
    - Lord Viridian: Luckly a lot of people here appreciate the entertainment.

> Viridian thanks the people at the ball for attending and officially ends the formal evening.

- Amryn cast [[Message]] to [Vosha](../characters/Vosha.md)
    - Amryn: I think we should talk in the interrogation room
    - Vosha: Yeah
- Sir Evan: I think you should get some rest and we can talk to my assailant.
    - Vosha: Yeah
- Sir Evan: Thank you for your help
    - [Elidee](../characters/Elidee.md): You're welcomed. I know that [Norman](../characters/Norman.md) is not too violent or unreasonable

> [Rircin](../characters/Rircin.md) wanders over to the door, as the crowd size is reducing and he **doesn't want to be noticed** by [Lord Samuel](../characters/Lord%20Samuel.md).

> Elidee and Xairan ponder what could've possibly made Norman stab Sir Evan

- Amryn cast [[Message]] to Rircin
    - Amryn: Should I maim Lord Samuel if I have a chance?
    - Rircin: No. We'll talk later about it.
    - Amryn: I do think we should have a conversation of which party members should have to get rid of.

> Xairan says good-bye to [Lady Angharad](../characters/Lady%20Angharad.md) and agrees to take her offer to visit them.

- Amryn: Sneakily goes to the downstairs
    - Elidee: [[Checks#Perception]] 22
    - Amryn: [[Checks#Stealth]] 20
        - Elidee notices Amryn walking down the stairs. Yells **wait up!**
- Amryn #cast [[Message]] to Elidee
    - Amryn: Don't wait for me. I have to do something
- Elidee: Amryn is staying back. Maybe **she's meeting a boy**.
    - Xairan: I think I'll give her backup
    - Elidee: She doesn't want to be backup.

# Back at home
- [Elidee](../characters/Elidee.md): You got cheese!
    - [Urist](../characters/Urist.md): I did get cheese
- [Rircin](../characters/Rircin.md): Oh, did you get some drinks too?
    - Urist: Look for yourself!
    - Rircin: Knocks on the barrel to see how full the barrel is.
    - [Norman](../characters/Norman.md), inside the barrel says: Come in!
- Xairan: I think it would be fun to push the barrel
    - [Xairan](../characters/Xairan.md) [[Checks#Strength]] 5.
        - Is **unable to push the barrel** with Norman inside.
- Xairan: Maybe we can put the anvil on top
    - Rircin helps getting the anvil on top

> A big thump on the barrel is heard by Norman.

- Norman [[Checks#Strength]] 15 to try to push the barrel out.
    - Norman doesn't succeed.
- Rircin: You'll **stay inside as a punishment**!
    - Norman: Can I get some food?
    - Xairan: We might add something for pickling!
    - Norman: How about something to drink?
    - Rircin: I can get you an ale!

> Norman is getting both ale and vinegar.


# Interrogation room after ball

> [Vosha](../characters/Vosha.md) enters the interrogation room, followed by [Amryn](../characters/Amryn.md)

- Amryn locks the door behind her.
- Amryn: Why did you target us?
    - Vosha: Money is easy
- Amryn: What do you know of me?
    - Vosha: That you're not a traitor
- Amryn: What do you know of [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md)?
    - Vosha: He was trying to frame you. Not the best of liers. Last I heard of him he was taken to a prison. There's **three places he could be**.
- Amryn: What about the rest of the party?
- Sir Evan:
    - Everyone else is safe
    -  Xairan should lay low. There's many eyes on him. Him getting social
- Amryn: You're not telling me anything I don't know. Your intelligence is not very good or reliable. It's that or **you're lying to me**


- Vosha had 50 HP

- Amryn
    - #cast [[Magic Missle]]
        - Damge 9 points
    - #cast [[Magic Missle]]
        - Damage 12 points
- Vosha: Are you done!? What's your plan here?

- Vosha: #cast Command
    - Amryn: Wisdom saved
- Amryn: #cast Magic Missile
    - Damage 12 points
- Vosha #cast cure 9
- Amryn [[Checks#Sleighthand]] 15 
    - Amryn runs away down the stairs
- Vosha #cast Command and succeeds

> There are guards comming in. Some at the foot of the stairs

![](../attachments/Pasted%20image%2020210110154958.png)

- Vosha #cast Command and succeeds again at Halting 

> Guards see Amryn

- Vosha grabs Amryn . Acrobatics 19 > 16

> Guards are approaching and are almost next to them

- Vosha: Good that you've stopped. It's time for you to do time!

# Cutting cheese at home

> Back at the safe house

- Elidee is talking with Xairan about smart Amryn is, and that she would be okay by spending a night with a boy until dawn
    - Norman: I didn't think Vosha was her type

> Xairan doesn't like this conversation and will keep looking for vinegar and lemons.

- Xairan squeezes them into the barrel
    - Elidee: Stop torturing him! Punch a wall or squeeze a ball instead!
- Norman: No one is listening to my side of the story

> Elide turns into a bear and will try to move the anvil to release Norman

- Elidee [[Checks#Strength]] 5 isn't capable of moving the anvil
    - Rircin helps to move it and the **anvil falls**
- Elidee picks up the barrel and drops it head first
    - Norman **takes 1HP damage** from falling to the floor


# Escorting Amryn to prison
> [Vosha](../characters/Vosha.md) is escorting [Amryn](../characters/Amryn.md) outside of the castle after having apprehended her during [Interrogation room after ball](Session_210110.md#Interrogation%20room%20after%20ball).


- Amryn cast [[Message]] to Vosha
    - Amryn: Sorry for this. I wanted to find out if you were honest.
    - Vosha: I don't know who is more incompetent. Your party or me for hiring you... 😭
    - Amryn: It's okay, I  wanted to get to prison

> The guards are taking her to the prison tower. As they drop her in a cell, they poorly checked her dress and **found nothing** on her
> ![](https://cdn.discordapp.com/attachments/689088005306449980/797844651464720394/WhatsApp_Image_2021-01-03_at_16.45.09.jpeg)

- Amryn calls out "Emlanor, is that you?"
    - Person: No, we did have one but was taken away. I think he made a deal. He was placed in the room next to mine.
- Amryn #cast Transe to sleep for a long rest in 4 hours.

- Amryn [[Checks#Perception]] 8
    - Doesn't see any guards in the prison
- Amryn uses thieves tools and opens the door
    - Amryn uses [[Disguise Self]] to dress up as another guard.
    - Another guard is coming down the stairs. Is suspicious of seeing another unknown guard so early in the morning and asks Amryn to wait at the keep after he checks stuff out.
- Amryn talks to the guards and [[Checks#Deception]] to get away
    - Amryn succeeds and **gets out of the prison building**


> Amryn is back in the city in the area around the prison.
> ![](../attachments/Pasted%20image%2020210110164742.png)

- Amryn manages to **reach the safe house** without an issue
   
# Party is joined again


- Xairan welcomes Amryn at the early morning
    - Xairan: Were where you?
    - Amryn: I was at jail.
    - Xairan: How?
    - Amryn: I got Vosha riled to get me to jail to look for [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md)
    - Xairan: I'm both angry, and impressed.
- Xairan: I need rest

> Xairan goes to sleep to get half rest

> Elidee meets with Amryn. They talk about being in prison and not finding Emlanor, but suspecting where he is.

- Rircin pours water on top of Xairan while waking him up.

> Party gets together to talk

- Rircin: What happened yesterday?
    - Norman: He killed my mom and brother
    - Rircin: Why didn't you tell us?
        - Norman: Deniability
    - Norman: My family were to perform for [Sir Evan](../characters/Sir%20Evan.md)'s knighthood coronation. There was some ruckus during the event, and my family was framed for it.
        - Xairan: What is going on?
        - Elidee: Why did you kill your dad?
            - Norman: Because him instead of helping us by talking to Sir Evan, he escaped and let them out to be punished by death
        - Elidee: What were they trying to steal?
            - Norman: Jewls, stuff... I don't know.

- Xairan: Why did you fight with Vosha?
    - Norman: He kept bringing up attention to it?
    - Xairan: How many people have you killed. Your dad, Vosha's dad... You tried to kill Sir Evan. We need to know for our safety.
    - Norman: Let's say I've killed 5 people. 2 Thieves, 1 Annoying guard and both my dad and Vosha's dad. **There shouldn't be more issues pursuing us in the future.**

- Amryn: I know that fairness is not guaranteed. My family have been murdered.
    - Elidee: There are consequences to the violence. Like with Xairan in the library. I can't believe I have to say this as the youngest person here.
    - Xairan: We can talk about morality later. I think we ca

> Vosha and Sir Evan have knocked on the house. After identifying themselves, Amryn runs away to hide.

- Sir Evan: Why did you stab me?
    - Xairan: Why is he too kind?
    - Vosha: Maybe he's taking pity on your skills
    - Norman: Do you want to try again?
- Sir Evan: I demand an apology
    - Norman: "Well, I'm *very sorry*"
    - Sir Evan: I think you're not mature enough to handle this. I expect of you to fix your squable with my squire. Why did you attack me over a fight with him?
    - Norman: To piss him off.
    - Vosha: Excuse me?
    - Sir Evan: I think your flippancy is too dangerous. I believe a **more public inquiry is required** from [Lord Viridian](../characters/Lord%20Viridian.md). 

> Elidee blocking the door stops Sir evan from leaving.

- Elidee: Do you know anything of the Kai Family?
    - Sir Evan: Are those a family of entretainers? 
    - Elidee: Did you ever interact with them?
    - Sir Evan: There were some accusations of stealing during my knighthood. I don't believe that I have met them personally.
        - Elidee [[Checks#Insight]]: 16. He does dismiss them easily.
- Elidee: What about you Vosha, do you know of them?
    - Vosha: I do not?
    - Elidee: I don't think you're being honest.
- Sir Evan: I don't recall much during the whole thing. There was a steal and a judgement. 

> I can be 

- Xairan: I don't think he would be a good squire for you.

> Vosha is terribly lying and 

> Sir Evan leaves the house and lets **Vosha alone with the party.**
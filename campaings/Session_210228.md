# Session 28-Feb-21

> Norman, Amrynn and Xairan are downstairs
> Urist is coming back from shopping

## Dredor
> Xairan said that if Amrynn and Emlanor would have an honest conversation about Dredor, the party would be able to get inside it.

- Amryn: What is it with you and Dredor?
    - Xairan: I don't want to be a knight. They had some info **I found necessary**. Maybe we should discuss about their letter first?
    - Amryn: Elidee found found a letter that says the following:

> Dredor's asstance the cause is procured, we have lost people. Please inverstigate, they have died nearer to your Kings city then yours. We will hold up our end of the bargain. You will have support from the East. We will be waiting. 
> Signed by: lord Aldar Talmor

- Xairan: I don't know this person. What East are they talking about?
    - Amrynn: Me neither. Something must've happened on this side of the mountains and made people angry. Maybe it's releated to Emlenor
    - Xairan: Could it be with the people Nebe killed at the house?
        - Amrynn: Maybe??
    - Xairan: "Dredor's assistance is procured". Maybe it was recent.
- Amrym: ... In relation to the throne of Evilan
    - Xairan: I think it's in **relation to myself**. I **think I am half orc**
    - Amryn: Well, at least I know what I am.
    - Xairan: There are some figures that want to drag this mysterious figure?
        - Amryn: Maybe they're looking for you to either be the king, or to kill you.
            - Xairan: I don't know what exactly.
    - A: What else connects you to Evilan?
        - X: I don't care about Evilan. I've been looking for info **on myself**
    - X: Fine. **I'll tell you**. I've been reading books (library). They mention about indivuduals that combine the strength of war and peace. Some cultures interpret that as races and or factions that have **blessings of gods** My plan is to get close to Dreddor to learn more about this.
        - A: So you need to know **who you are and how much you want to run away**
        - X: Yeah. Also, [Lady Angharad](../characters/Lady%20Angharad.md) might be involved. I'm not sure if she's aware of this prophesy. I don't think she's my mom.
- A: If we go to dredor, go to the **largest house and ask for the maid**

> Xairan goes upstairs to deal with Norman and Emlanor

## Dealing with Emlanor

> Norman describes Emlanor as *euphoric*
> Xairan cast "Ray of Frosting" on Emlanor

- A: What's going on?
    - X: Emlanor is being non-cooperative
    - A: I'm coming up
- Urist: What's going on?
    - A: There's 2 boys fighting over me upstairs
    - Urist: Who could be **interested in you?**

> A fight breaks out. The members now **roll initiative**


- U: Tries to jam a whitesheet to Xairan's head
    - X avoids it

## Shopping

> Amryn is borrowing the hat of disguise to go shopping

- Amryn now has a slightly malfunctioning hat of disguise. There's a 1/6th chance that it will not turn into your desired 

> The party gains a Driftglobe, a Wand of Fear a Malfunctioning Hat of Disguise and some silverware from seemingly Evilan


# One-shot

> People roll perception
> There's an eerie high pitch noise that is being maintained.

- A: Tries to open and close the door.
- N: Ignores it and goes back to bed.

> There's a raised platform, with 4 columns. You can't really see any roof

- A and N cast Blade Ward
- U tries to wake up N
    - They're awoken and go downstairs

> The party has all their swords and weapons at the ready.
> Amryn takes the lead into going to the void

![](../attachments/Pasted%20image%2020210228170802.png)

> The party has been standing idle for too long. The party has to [[Checks#Dexterity]]. They all managed to duck away from **something that rolled at them**. A log of spikes that came at them

- X: Throws ray of frost and fireball to the light
    - There's no

> The party fell, following Xairan into the water

![](../attachments/Pasted%20image%2020210228182433.png)

Out of the water comes out a form made of water and forms. It's a **water elemental**

> At the end, it seems that **only Xairan remembers** this event from happening.
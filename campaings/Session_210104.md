# Recall

- Norman stabbed [Sir Evan](../characters/Sir%20Evan.md)
    - Norman ran away
- Vosha lost 2HP
- Norman and Vosha were getting tired of eachother
    - Things got heated up while they were separated from the party.
    - Vosha threw a knife at Norman
        - Lady angarad attacked me with misile
        - Xairan threw a blinding spell
            - Vosha is angry at Xairan because of that

- Party is no longer in combat
- Sir Evan was healed by Elidee
- Xairan managed to get the governor to only think its a personal feud
- Elidee managed to help Norman to sneak away from the party back to the city.
# Last session
- Elidee and Rircin were on the way back to the safe house
- Amryn was waiting at the safehouse
- Urist and Norman were succesful at hunting. They dropped stuff into a tannery to get some hide and trophies.
- Xairan bought acid

# This session
> The crew is united again in the safehouse

- Xairan: I got acid everyone!
    - Amryn: Don't spill it!
    - Rircin: Why acid?
    - The acid is used to reveal messages that have been hiding in a secret book
- Amryn: Oh, we're on escort detail for a rich person at the ball. We got 200GP and we expect 50GP after protecting the person.
    - Rircin: Is that why there's guards outside?
    - Xairan: It's because they have Amryn, as a wanted criminal
    - Rircin: That diplomat friend of yours lied and got and himself in trouble. It's probably
        - The green-blue guards caught him.
        - Amryn: I won't help him on this one.
- Rircin: What happened to us was
    - Your friend pretended to be a big spy master. Started ratted us out.
    - Elidee: They've been looking for people acting suspiciosly in the city. He mentioned himself and also that Amryn should also be suspicious.
    - Elidee: I couldn't meet my mom. Some soldiers came in
    - Rircin: We met a knight who was also waiting for Elidee's mom, but we couldn't find her. That knight might be from a group opposing the green-blue guards.
    - Xairan: We think that your mom might've flipped sides. Turns out that [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) and [Coraline](../characters/Coraline.md) might've been working together to de-stablize the current governor. The name and description match that.
        - Elidee: I would appreciate if you wouldn't **accuse my mom of treason**
        - Xairan: I'm not, but people think that of her. I understand that it can be hard to accept the possibility. She's been gone for so long, things change over time, they discover new information.
        - Amryn: Same with Emlanor. I can't trust them anymore.
        - Elidee: I will hold off on distrusting someone I've know all my life.


> The crew takes a long rest. Clothes can be picked up today. The day is to their disposal. The ball is **tomorrow**, so today is the last chance for the crew to get ready.

- Xairan: Can you teach us to pick locks?
    - Amryn: [[Checks#Inteligence]]: 15, [[Check/Charisma]]: 1. It takes an hour for them to learn how to pick a basic lock.
    - Amryn: I think I'll go to buy you some lock picking tools

## Rircin and Elidee get clothes

> The crew goes to pikes. Elidee is immediately recognized

- Elidee: My friend and I need nice clothes
    - Clerk: We have a great assortment of colors and styles! We can also make sure it has pockets
    - Elidee: Could you reinforce it?
    - Clerk: We can add some layers of leather but it would cost 1GP to have it ready for tomorrow. 
- Rircin: I will pick the orange one.
    - Clerk: Total it will be 16GP each, but as a friend of [Coraline](../characters/Coraline.md) we'll make it 15GP each.

- Rircin [[Checks#Perception]]: 24
    - Notices the 9-fingered halfling trailing them

## Xairan and Norman gets magic stuff

> Xairan reaches a *bougie* store called the Green key. It appears to be a very stylish store where everything is selective. It's tended by elves wearing yellow. There's many odd looking hats with no visible price, as it is the nature of the audience they expect.

- Xairan feels out of place in the store but can't figure out what he wants]
- Norman is looking for pointy things, but doesn't seem to find anything of interest.
- Xairan approaches the counter to ask about the products
    - Clark: There's a green hat that makes you go unnoticed
    - Clerk: This hats can get you control of a bat?
        - Xairan: Can I infect this bat beforehand?
    - Clerk: We also have potions. Some of great healing.
    - Xairan: Elidee might want a hat of disguise. Do you also have a secret collection?
        - Clerk: Taking it as an insult. This store is not for travellers, but for high class
        - The hat costs 24GP
        - The potion costs 60GP

- Norman [[Checks#Perception]]: 6
- Xairan [[Checks#Perception]] 8
    - Don't notice anyone out of the ordinary

## Amryn get lock picking tools

- Amryn [[Checks#Perception]] 6
    - Don't notice anyone out of the ordinary

- Amryn goes shopping


## Urist holding the fort

- Everything is fine at home


> The crew meets back at the safe house

## Elidee look for Oba with Rircin

- Elidee is uncomfortable with the thought of her mom being a traitor, specially if it's something that they read on a book
    - Rircin understands and would like to also be able to clear her name
- Elidee would like to catch up with [Oba](../characters/Oba.md) and 


- Elidee turns into a creature with high sensory perseption (Ferret)
    - Oba doesn't go to her home, but rather to a bar
    - Inside the bar, Ferret Elidee see's Alistar and the Knight she had met when looking for her mom in [Session_201101](Session_201101.md)
- Alister, it's sad that we bring you this news. We thought we should treat you for this.
    - Get me something strong
    - I'm sure your husband will come back in one piece! This time we don't believe he won't be there for long so we need to get them out quickly
    - The gnome feels guilty for not providing the right intel the first time.
    
> It's probably that Oba's husband is locked up and may have also been locked up along with Elidee's mom.

- Elidee tries to **remember ths scent** of them

## Xairan goes to the Bar again

- All the big names are coming. Knights integral to the safety of Ardian, Edwinem, Alastar, Evan and someone from [Dagonaut](../locations/Dagonaut.md)
    - The people from Daganout **wear yellow**
    - There will be guards coming to the bar after the ball, and we'll learn all the gossip from them.
- I might need some help in 2 days after tomorrow for support at the bar.

  
## Amryn 

In disguise, Amryn goes to meet the first person they met when they arrived to the [City of the South](../locations/City%20of%20the%20South.md). Mr [Kurr](../characters/Kurr.md)

- Amryn asks about [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md).
    - City guards were looking for him. Someone ratted him out. Guards came looking at my place, but everything was clear on my end
    - Amryn: that's also why I'm in disguise. Do you know where he could be?
    - I've heard that he's at the main watch house from the governor watch **not the city watch**
- Amryn tries to sneak in back to home
    - Amryn [[Checks#Stealth]] 12
        - Manages to sneak in succesfully to the 


## Xairan goes to library

As Xairan arrives, he notices that someone has been sweeping the library that was ransacked.

- Xairan asks the person whose inside
    - I'm the wife of the librarian. I'm helping to clean up
        - My husbandd was taken by the guards because they were looking for information. I expect him to get back tomorrow.
        - The guards said that the library was being *used to pass messages*

- Xairan [[Checks#Insight]] 19
    - The lady is **genuenly distraught over the whole experience**
- We think that my husband was taken by the guarsd in blue-green.

- Xairan goes back to the shelf where he found the previous book.
    - Xarian [[Checks#Investigation]] 20 (nat)
        - Finds an oil vile that would've light up on fire
    - Xairan #cast a freeze ray
- Xairan explores the books available in the shelf. None of them appear suspicious
- Xairan, bored, decides to light up the trap anyway.	
    - Xairan lets the oil melt, puts it back into its location and sets up the trap
    - The shelf and all the dried pages start lighting on fire. Pieces are of the shelf are burning. The books on top burn first.
	- Xairan grabs some books he thought interesting from  the shelf
        - Forest of Dagonaut
        - ???
- Xairan casts firebolt and runs to the front of the library looking for help
    - "Fire fire! someone *is trying to set the library on fire*"
- The librarian's husband goes outside and screams for help
    - Rapidly, a line of volunteers start bringing buckets of water
- A **city guard** pulls Xairan and the librarian to the side to ask them questions
    - Xairan tells a story of knowing how the trap started [[Checks#Deception]] 8
    - Xairan tells the guard what the location he's staying at ([Kurr](../characters/Kurr.md)'s place) and has been working (, 
- As Xairan is leaving, 4 **governor guards** arrive the location
    - Xairan [[Checks#Perception]] 7
    - One of the governor guards appears to recognize Xairan from when he was interrogated by the guards that was called off by [Vosha](../characters/Vosha.md)
- Xairan walks into the next side street
    - Xairan [[Checks#Perception]] 14
        - Guards are approaching and are asking him to stop
    - Xairan claims that he didn't set up the trap [[Checks#Deception]] 11
        - The guards recognize that he's been in the 3 window house road
    - Xairan reluctantly agrees to be escorted by the guards.
        - Xairan is now at the **watchouse where Elidee lies**
- Elidee [[Checks#Perception]] 17
    - Elidee spots Xairan being taken by the guards
	- Elidee **sends a message** via an owl to Rircin whose at the safehouse saying that Xairan is being escorted by city guards.
       - Xairan is being taken to the main city watchtower, near the Bored Goats'Inn, by 4 Governor's guards. Don't know why. I am following
- Amryn is frustrated at the party's irresponsibility.

# Continuation of Fox and Goose 

> Elidee, Rircin were [Riding out](Session_201101.md#Riding%20out). Met a [well dressed knight](../characters/Alistar.md) and a gnome. On their way back from the [Fox and Goose](../locations/Fox%20and%20Goose.md) back to the [City of the South](../locations/City%20of%20the%20South.md)


Something happened in between. Currently, are [Elidee](../characters/Elidee.md) in Spider form and [Rircin](../characters/Rircin.md)

- Currently at the back gate, Rircin approaches the guard
    - The guard is suprised to 
- Elide [[Checks#Perception]] 19 while inside the gnome riding donkey 
    - The gnome on the donkey 
    - The knight is reachining for a knife inside their book
    - Elidee positions herself into defensive
- Rircin [[Checks#Deception]] w/adv 14
    - The crew is **let through into the City**
- The knight and Rircin part ways
    - The knight says that will go and stay inside the castle. 
    - The knight will also use the information that Rircin gave him as he thinks it might be something disconcerting.
    - Rircin gives the address of the [Safe house](../locations/City%20of%20the%20South.md#Safe%20house) to the knight.
- It is nightfall
    - Elidee [[Checks#Perception]] 11 to undisguise herself
        - There's noone around
        - Elidee turns back to regular form
- Both split apart on their way back home to 
    - Both [[Checks#Perception]] 
        - Elidee 1
            - Doesn't think she's being followed
        - Rircin 22
            - Doesn't appear to be followed
            - There are some [Blue and Green](../misc/Blue%20and%20Green.md) wearing guards. They seem to be looking at the direction of where Rircin is, **not necessarily looking at him**
            - The 9 fingered halfling that was sent by [Vosha](../characters/Vosha.md) isn't near the home either.
    - The crew is **on their way home**.


# Continuation of Library

> After going shopping to get clothes for the ball. The crew found a ransacked library. They found a woman called [Ms Freia](../characters/Ms%20Freia.md). A message was found. The crew did some chemistry and found a larger message, but wants to reveal even more of a message. The Xairan would like to buy more acid

The crew is at home, sortof mid-day.

- Xairan would like to buy acid
- Amrynn would like to tidy up the hiding places of the party secrets. She's afraid of having **easily searchable incriminating evidence**
    - Maybe some of the space on the attic
    - There isn't anyone in the party who can make a wood chest
- The party plans to **split up**
    - Amrynn thinks of staying inside to avoid risky encounters, so hiding the notes for her
    - Xairan wants to buy acid
        - Amryn would like them to buy stuff for her
            - Her "message" skill isn't strong enough to communicate throughout the city.
        - Norman wasnts to go outside
        - Urist will go outside too
            - Norman and Urist will go to hunt a bear equipped with **bear fists**

>  It seems that the more chaotic members will go outside on their own

![](../attachments/Pasted%20image%2020201107154359.png)

> There are three small gates around the periphery of the wall and three main entrances (2 at front and 1 at back)

## Acid hunting
- Xairan walks around the city
    - Xairan reaches a neighborhood known for their alchemists 👩‍🔬 and their colorful workshops with smoke.
    - Xairan approaches a workshop that emanates **blue smoke**
        - The entrance is a bit low
        - A halfling woman is tending the store
    - Xairan [[Checks#Investigation]] 12
        - Notices that on the ceiling there's a lot of tools hanging on the ceiling	
        - Xairan appears to be very large for the environment, which also has the benefit of making him **imposing**
    - Xairan asks for the *head alchemist*
        - Xairan claims to have experience with lemon juice and acids and wants to test if lemon juice is a weak acid.
        - Xairan runs a risk of mansplaining what safety is to this half-ling alchemist.
        - Xairan [[Checks#Deception]] 24
            - Xairan would like to keep the trade secrets among alchemists.
            - Xairan offers 2GP to keep it a secret
    - The halfling accepts the offer and even suggests sharing a decanting flasks
        - Xairan reveals that the color of smoke blue is what attracted him to the shop, some connection between copper and blue, therefore a valuable establishment.
        - The halfling hands over three flasks of different color
            - Red: Carbolic
            - Blue: Hydroxic
            - G: Sulfuric
    - Xairan calls himself **Zarathustra** [[Checks#Deception]] 20
    - The halfling reveals that they were working on a **smoke bomb** and are currently tuning it for it to last longer #unresolved-thread 


## Amryn in the attic

- Amrynn goes up to the attic
    - Amryn [[Checks#Investigation]] 3
        - There are some clunky locks and chests
        - The place is dusty
    - Amryn thinks of replacing locks in non-obvious ways of currently existing chests
        - Amryn [[Checks#Dexterity]] 4
            - The old lock breaks, but **at least the chest is opened**. 
            - It might be possible to make a new lock for the same chest or try another chest
- Amryn tries again with another lock
    - Amryn [[Checks#Dexterity]] 17
        - She succeeds at **making a strong long on an old looking chest**
    - Amryn builds a fake bottom panel
        - Amryn places **all her secret papers** under the new hidden panel
        - Amryn puts all the precious metals on top of the panel
            - The goal is so that people only pick the valuables, and ignore the fake bottom
- Amryn now goes downstairs to see if there's bags left around
    - Urist is stil carrying weapons
- The forge is not operational until tomorrow.
    - Amryn will now wait until

## Urist and Norman go hunting
- Norman and Urist will look for a bear
    - It seems that bears might be too far away, so they're willing to settle on hunting ducks 🦆 😡
    - Norman [[Checks#Perception]] 14 
        - The [Blue and Green](../misc/Blue%20and%20Green.md) guards are okay to leave people to exit via the gate

![](../attachments/Pasted%20image%2020201107160521.png)

- Urist will take on the duckling 🦆 and Norman goes for the bear 🐻
    - Their first step is to get out of the road
    - Urist [[Checks#Nature]] 20
        - Urist is **one with the nature**
        - Urist sees the areas where moles, ducks, rabbits and **small animals would be**, and potentially some deer trail
        - Norman thinks that small animals are too few, and they should try to find an elk or a deer.
    - Norman [[Checks#Survival]] w/advantage: 19
        - The team gets on a path to trail deer.
        - They've reached the end of the path, and now it's mostly forest with needle trees. In this area it might be posible to find more **predatory animals**
            - Urist and Norman need to decide their strategy
- Norman and Urist want to go to the predator area
    - Norman approaches **loud and bold**
        - Norman [[Checks#Survival]] 9
    - Urist takes a more calm approach, expcting Norman to be bait
        - Urist [[Checks#Survival]] 9
        - Urist [[Checks#Stealth]] 4
    - They encountered loud foilage as they were walking. 
    - As they were walking they find a single boar 🐗. It is in front of them, not turning back, but not attacking either.
        - Norman tries to hurt the boar #cast Toll the dead
            - It **failed**
    - Boar attacks back
    
> Audio for Enrique has cut-off at this point... The suspense is killing me!
# Recap
- Vosha is left with the crew after Sir Evan
- Amrynn is hidden at home after breaking out

# Session

## Interrogating Vosha

- Vosha: You know that my days are numbered.
- Urist: Why did you hire them
    - Sir Evan is King aligned
    - The governor (King's child) is plotting against Sir Evan, and particularly the King
- Vosha: Lady Angharaad is king aligned with **us**
    - Xairan: That doesn't make sense. We have info that she might be responsible for putting the governor on his current position. They have letters.
- Elidee: Do you know an Imon
    - Vosha: No, it doesn't sound familiar
- Xairan: Oba has been using the library to share messaages
    - Vosha: That's new information to me.
    - I think she is looking for information religious books to find legitemacy of lineages to support the governor as a rightful ruler as a child of devine beings.
    - Elidee: I'd like to point out that... These are not people conspiring against the king.
    - Rircin: Maybe he's looking to take evilan.
- Elidee: We know that the governor was increasing the number of guards outside of the castle while the ball was takings
- Elidee: I'm not on the side of the governor. He's attacked commoners
    - Urist: sounds like a dick too
- Xairan: Why do you know that Oba is on our side?
    - Elidee: I've been following her about and gather info on us. She was one of the last people to see my mom.
- Urist: You don't have to be aligned with the king to be against the governor. He's a bit of a dick.

- Vosha: Some people on our side are [Oba](../characters/Oba.md), [Alistar](../characters/Alistar.md) and my Halfling (with 9 fingers).

- Vosha: Here's a location where you can meet my **halfling on midnight tonight**

> Xairan #cast Ray of Frost as they thrown Vosha out of the house.

## Party talk
> Amrynn exists the hiding place

- Xairan: I think we should hunt the halfling friend of Amrynn
- Amrynn: Just so you know, I went to a jail and learned that [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) is not at the prison I was at.
    - There are rumors he's been spilling out the beans to the governor's guards
- Rircin: [Lord Samuel](../characters/Lord%20Samuel.md) and I have a complicated past. I grew up on his castle. I don't trust whatever side he's on
    - Xairan: Sounds like he is only on his own side.
    - Rircin: He should be a Lord who protects those weaker than himself.

- [x] Get Emlanor while on prison
- [x] Meet the halfling
- [x] Intercept Lord Samuel when he's leaving the city

- Elidee: We can't keep ignoring all of these powerful people that harm, we just keep running away from consequences.
    - Amrynn: We'd likely keep running for life.
    - Elidee: Know that the actions we take also burn things on our wake.

> Elidee convinces people to come clean about their lineage

- Xairan: My motive is to **know about my lineage**. I don't care too much about this plot, until the point that people also care about lineage to overthrow kings
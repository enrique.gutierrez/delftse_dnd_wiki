# Introduction

Last time, there were some hijinks in the [City of the South](../locations/City%20of%20the%20South.md). There was a person seeking work in a tavern.

There were people looking to build a forge but haven't decided where exactly it will be placed in the [Safe house](City%20of%20the%20South#Safe%20house) #unresolved-thread

- [Amryn](../characters/Amryn.md) There was someone who wasnt wearing the right face with a meeting with her.
- The crew took a long rest and they're all at the safe house.
- [Elidee](../characters/Elidee.md) has a meeting with [Oba](../characters/Oba.md) and [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) with a necklace.
    - Elidee wanted to stalk Oba
- There was a meeting with a squire that was mentioned in [Session_200523](Session_200523.md)

# Session

## Stalking Oba

- Elidee is in [Wolf spider](Elidee#Wolf%20spider) Form
- [Oba](../characters/Oba.md) lives in the northern area if the City
    - Elidee followed Oba to her place
- The house looks like she lives all alone
    - It's a single story house
- Elidee looks for details around the house
    - Elidee [[Checks#Investigation]]: 12.
    - No extra hiding space looks to be available.
- Elidee waits to see if Oba is contacting other people.
    - Elidee [[Checks#Perception]]
    - Oba looks to be talking, despite living alone.
    - She might be sending a message, animals or an undercover person.
- Elidee goes back to the [Safe house](City%20of%20the%20South#Safe%20house)


## Goes to meet Emlanor

- [Amryn](../characters/Amryn.md) meets with a familiar half-elf [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md)
    - HE: I am sorry to left you half-way.
        - AM: Last time we met we were diplomants
    - HE: I hope I don't have to do something that I'll regret
        - AM: Is there something I should know?
        - HE: Theres a group [Navin](../characters/Navin.md) Who are making sure the peace in eemland is more fragile than iniially expected. Theres a person whose agents are colluding with parties in [Unahuen](../locations/Unahuen.md) to weaken his current position.
    - AM: people look to be more afraid in both city of south and unauen to be more sensitive to weapons.
        - HE: Don't go around with weapons
    - AM: Can you help me get into the ball?
        - HE: I'm planning to go, but you need a good reason to be there. I will consider it and be back 6 days before the ball. #unresolved-thread
    - AM: I am traveling with [Coraline](../characters/Coraline.md)'s daughter (Elidee)'

## Nightly moment (yesterday)

> We take a narrative device to go back in time and show what happened yesterday night...

### Safe house
- [Elidee](../characters/Elidee.md) meets [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md)
    - The Emlanor can carry a message to [Coraline](../characters/Coraline.md)
    - Emlanor is meeting Coraline tomorrow at the [Fox and Goose](../locations/Fox%20and%20Goose.md)
        - ES: You should be sneaky
            - Elidee turns into a spider to show of her sneaking skills.
            - *Emlanor is impressed*
    - Elidee can come, but must hide and run away on command from Emlanor

### Rich people area
- [Xairan](../characters/Xairan.md) want to visit the rich people area of the [City of the South](../locations/City%20of%20the%20South.md)
- Xairan is looking for carriages
    - The carriages are seemingly from the working class
- Xairan is seraching for "town criers" (the people that share news)
    - There don't seem to be any at this point of the night, as citizens don't like to be disturbed.
    - Some are hanging in a corner. Two gnomes, and a human.
- Xairan talks with the criers
    - Asks to bet with the human crier on [Sir Evan](../characters/Sir%20Evan.md) vs [Sir Marwin](../characters/Sir%20Marwin.md).
    - Xairan claims to be a man of science.
    - A discussion goes further on the bet. Talking about details of who was hurt and when.
        - Bron is the name of the crier
    - The criers wonder what is the interests of Xairan in the city.
        - Xairan mentions about changes in the attitude around city.
        - Bron accepts the city has changed, but is still suspicious of Xairan's motives.
            - Xairan [[Checks#Insight]]: 1+2
                - *Xairan believes that*
    - Xairan unsubtly tries to bribe with 1 gold coin the crier
        - Bron refuses
    - Xairan tries to intimidate them by lighting up his hands
        - Xairan [[Checks#Sleighthand]]: 5-1
        - Xairan [[Checks#Intimidation]]: 12+3.
            - *Every one sees Xairan light up his hand.*
        - Bron stands his ground. A hand goes behind their back. Starts counting to 10
            - Xairan [[Checks#Perception]] 10+0
            - Xairan cast [[Blade Ward]]. No reaction from Bron
    - Xairan [[Checks#Insight]] 3+2
        - Bron replies with a **sick burn** on the odds being against Xairan the man of science.
        - _Xairan gives up and goes back home_
    - Xairan [[Checks#Perception]] 4,
        - **There's nothing noticeable on his way back**
        - People might have followed without his knowledge. `¯\_(ツ)_/¯` #unresolved-thread

## Morning

> The narrative device ends, and we're back to regular schedule.

- [Elidee](../characters/Elidee.md) tells the rest of the crew about her plan from yesterday night

### Search for Squire
- [Rircin](../characters/Rircin.md) thinks about the Squire ([Vosha](../characters/Vosha.md)) who was mentioned should meet once they arrived to the City of the South.
- [Urist](../characters/Urist.md) and [Norman](../characters/Norman.md) want to join on the search for the Vosha.
- The kights they encountered on the road gave them a hint about Vosha
- They go towards the [Castle](City%20of%20the%20South#Castle)
    - They pass by a market
- They get to the castle
    - There's guards around it
    - Urist [[Checks#Perception]]: 8.
        - The guards don't show any allegience by what they're wearing
        - Colors are green, blue & burgandy
    - They try to walk around the guards. They reach the courtyard.
- They ask about Vosha to a guard in the castle
    - They must drop all weapons before going inside
        - They consider dropping the weapons first.
        - The guard lets them in to a first inner area keeping their weapons
        - The guard goes inside to call on Vosha
    - Crew [[Checks#Perception]]: 20+0, 8+0, 20+2.
        - There are archer guards on the inner walls prepared, not aiming at the crew.
        - Guard states that the archers are justified as there are great forces outside the city
        - The guards don't want to discuss if they expect a riot in the city.
- Vosha is pleased to **meet the murderer of his father**
- Norman and Rircin recieve invitations on their names.
- The crew goes out of the castle
- A small half-ling is following them
    - DM [[Checks#Stealth]] 1.
    - They notice the half ling following them very poorly
- The crew tries to split up and catch the half-ling
    - Urist [[Checks#Stealth]]
        - *Goes around behind the half-ling and taps them on the shoulder.*
    - Urist [[Checks#Dexterity]] 12+1.
        - *She tries to hit Urist but fails*
    - Urist [[Checks#Strength]] 19.
        - *He grapples the halfling.*
- The half-ling poorly denies following them
    - Urist, calls on Rircin and Norman for help
    - Norman threatens the halfling
- The halfling tries to run away.
    - DM [[Checks#Dexterity]] nat 20.
        - *The halfling breaks free.*
        - *The crew runs behind her.*
    - The halfling ducks nearby and hides behind some objects
        - Urist [[Checks#Perception]] 19
            - The halfling is bad at hiding. And the crew surrounds her.
        - The halfling takes out a vile of substance
            - Urist and Norman [[Checks#Investigation]] 4 to know the content of the vile.
                - *They don't know what it is*
            - Rircin [[Checks#Insight]] 12.
                - The halfling believes the potion will get her out.
        - Urist [[Checks#Intimidation]] 15.
            - The halfling tries to drink the vile
        - Urist [[Checks#Dexterity]] 11.
            - *Knocks the vile and steps on the vile.*
- Norman and Urist are tired of the half-ling and are threaten her with dismemberment
	- Norman [[Checks#Strength]] 12
        - *Halfling avoids the grab*
    - Norman [[Checks#Strength]] 8
        - Halfling pities Norman and says: "*Better luck, next time*"
    - Norman [[Checks#Strength]] 11
        - *Halfling is caught*
    - Norman aims to cut her finger [[Checks#Attack]]: 15 with 3 damage
        - *Halfling gets a left pinky finger cut.*
    - Norman asks her why they're being followed
        - Halfling thinks that they're not very smart to realize who sent her
        - The halfling admits that it was someone from the castle they just came from
        - The halfling admits that it was a **squire who sent her**.
        - The halfling only wanted to know where the crew lived.
- The halfling is released. Tells them that she will keep an eye on them.
    - The halfling keeps following the crew but not sneakily.
- The crew goes back to the safe house.
- Norman kept the finger.
- Once back to the safe house Urist tells the crew that the spy is there around the house.

# Upcoming activities

- [Elidee](../characters/Elidee.md) is planning to go alone to meet the mother #unresolved-thread
- [Amryn](../characters/Amryn.md) wants to set up the smithing with [Urist](../characters/Urist.md) #unresolved-thread
- [Xairan](../characters/Xairan.md) has a sidequest on his own. #unresolved-thread
- [Urist](../characters/Urist.md) and [Norman](../characters/Norman.md) go for game night #unresolved-thread
# Recap
- Norman, Xairan and Amryn went to the prison where [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) is held
    - They fell a bit down the rope
    - Xairan disguised himself
        - There are currently 2 Emlanors in the prison
    - Norman is standing guard
- Urist, Elidee and Rircin are on the castle talking with [Alistar](../characters/Alistar.md)
    - They're told to go to the library
# Session

## Going to the library

> Party
> - [Rircin](../characters/Rircin.md)
> - [Elidee](../characters/Elidee.md)
> - [Urist](../characters/Urist.md)

> They go back to the library that were originally denied a during [Session_210131](Session_210131.md)
> They see that the prices were incredibly high for **non-members** to the library, luckily their new **signed letter of patronage** obtained from Alistar, helps them go through.

- Rircin goes to the reception desk and shows them their signed letter
    - The receptionist checks the letter and determines its genuine
    - The receptionists requests them to fill in papers and pay for a membership
- [Omlau](../characters/npc/Omlau.md), a librarian talks to the party.
    - We have books
        - Botany
        - Magical Animals
        - Non-Magical animals
        - Non-magical  magical animals
        - History
        - Fictional history
- Elidee would like to look for
    - Factions of evilan
    - Lineages of Evilan
    - Info on the White Hills

- Lineages
    - ![](../attachments/Pasted%20image%2020210221114648.png)
- Magic
    - Omlau: You have to go to the stores.
- Maps:
    - Omlau: We have a map, just **make copies of them**
- Regions on White Hills:
    - DM: Borders between Unawen and the Grey Hills are very discrete.
        - Evilan has a bastion of 
        - The Grey Hill is a bit cultish?
- Rircin is looking for [Lady Olwin](../characters/npc/Lady%20Olwin.md)
    - Omlau: She is **not here today**
    - Rircin [[Checks#Insight]] 5
        - Rircin belives Omlau despite seeing a well dressed lady in the history section.
- Factions of Evilan
    - ![](../attachments/Pasted%20image%2020210221121440.png)
- Magical creatures
    - Dragons egg interactions
        - How did Xairan came by that eggs
    - DM: It's been known for dragons to interact with humans when the mother's are not able to interact with the egg.
    - DM: It could be that the mother dragon is **persuaded to be away** from the egg, not just necessarily killed.

> Rircin suggests to Omlau for ideas on how to make extra money


## Shopping

- Elidee would like to buy
    -  stuff like food
        -  3x days of rations 
            -  That would be 72 SP
    -  
- Lances
    - 
# Recollection
> The party has picked up their clothes.

The party is now discussing their battle plan for the ball
- Amrynn: No maiming or killing people?
    - Norman: What about scarring?
    - Rircin: Only mentally
- Rircin: Look Norman, before doing something, think "would Rircin do this?"
    - Urist: Isn't this an invitation for loopholes?
    - Xairan: Well, luckily I am not the thinking kind.

The party has equiped their outfits and some short concealable knives. Their armor is now **leather class**.

To prepare for the ball, some final preparations
- Elidee #cast bark resistance.
- Elidee [[Checks#Dexterity]] 13.
    - applies some minor makeup to Amrynn which is functional enough
- Xairan would like to wear his cape of disguise.

# Ball

## Arriving

The party is walking
There are carriages approaching the castle

The doors of the castle are guarded by city guards. They don't check the people of the first entrance.

- Rircin [[Checks#Perception]] 14 to know if the guards are nervous
    - The guards and the city seem determined and always in groups between 2-5 guards.

## Courtyard

![](../attachments/Pasted%20image%2020201213133807.png)

The carriages are being parked. No vehicles are allowed passed the second gate in their vehicles.

A guard approaches the party and checks their invitations. Everything seems to be in order and given instructions.

The path seems to be crowded with well dressed people. 

There are defensive structures in the courtyard. A grass lawn and a water well. The main building square is 3 stories high, with a couple of 1 floor high building on its sides.

- Xairan [[Checks#Investigation]] 10. He doesn't see much

> Elidee and Urist leave the party to go to the city.


## Into the ball
- Amrynn has a green ball gown. Also carrying daggers and lock picks

![](../attachments/Pasted%20image%2020201213135804.png)

- Vosha introduces himself to the "reservior dogs"
    - Vosha tells the crew about the people there

## Food 

- People are sitting down
- Norman is sitting between Rircin and Amrynn. They seem to want to stop him from rash actions
- Norman #cast detect poison.
    - No poison was found


- Sitting next to the crew are some dwarves who are escorts to [Lady Dervla](../characters/Lady%20Dervla.md)


## After dinner

- Viridian stands up and is about to start a speech
    - We have all taken steps for the betterment of this country. My father was a very wise man to grant me this position. We're grateful for his insight. I welcome Lady Angarad to have helped me be in place. To Lady Dervla I thank for showing the fruits of diplomacy. I hope to speak to all of the guests. I'm glad to see that you're all still allies to our cause. I would like a toast to our king.
- The party gets up to talk and mingle with the guests
- Amrynn and Xairan 
- Lady Angarad are talking dwarvish
    - Amrynn can overhear and notices the conversation. It's only about the joy of exchanging ideas across regions.
- Sir Evan, Norman and Vosha are having a conversation
    - Sir Evan feels that there's a good amount of allies in the ball
    - Norman introduces himself, at the Vosha's displeasure.

> Roll perception

- Everyone [[Checks#Perception]] 
    - Vosha: 15
    - Norman: 11
    - Xairan 2
- Those who rolled over 10, notice that Addie is no longer wallking alone the hall. She's standing next to another person. There's another person in the opposite corner.
    - Overall, there's an increase of governor guard looking people.
- Amrynn sends "message" to the rest of the party.


- Sir Evan and Lord Samuel are talking
    - Vosha and Norman want to listen in
    - Addie approaches Norman and starts talking to him
        - Vosha [[Checks#Perception]] 6
        - Norman [[Checks#Perception]] 11
    - Norman: What do you think of Sir Evan?
        - He's a competent knight
    - Norman: What do you think of Vosha? You're glancing at him a lot.
        - I haven't seen you before.
        - Norman: Neither have I. (gives no info back about Vosha and their arrangements)
    - Norman and Addie are both getting close to Evan and Samuel
    - Sir Evan and Samuel are talking about crops and how well they're going. They seem to be happy with the status quo. 
        - Samuel would like to have better weather. 

# City

Urist and Elidee are walking around and see what **they're trying to supress** something.

> They put on normal clothes.

- Elidee [[Checks#Perception]] 22
    - Notices that as they're walking around
    - A good number of guards are going in the direction of the safe house.
    - Some guards are going to the direction of pikes
    - Some guards are going to the direction of the library
- The party **follows to pikes**
    - It seems that the guards were going in the direction of [Oba](../characters/Oba.md).
    - The leader joins the guards. It is now 8 people.
- Elidee thinks that this is the place that she gave the guards when they saved Xairan
    - Urist thinks that the guards were looking for both of their homes.

- Elidee [[Checks#Perception]] 9
    - Before going into the door it seems that the boots inside the place and can't hear the sound of voices.
- Urist staying behind [[Checks#Perception]] 18
    - The neighboring houses have been woken by the raid
- Elidee [[Checks#Stealth]] 7
    - The guards see the wasel. Their instinct is to *shoo* the weasle off.
    - Elidee moves quickly into the house further
- Elidee [[Checks#Perception]] 5
    - Elidee is quite busy running in between legs that she can't really percieve things of what's happening inside the house.
- Elidee goes further inside the house [[Checks#Stealth]] 8
    - Hides in between the walls
- Neighbors are coming into the street and 
    - Urist [[Checks#Perception]] 3. Can't make up what they're saying
    - Urist [[Checks#Stealth]] 8: He get's closer
    - Neighbor: It's just the children in there!
        - Guard: We have reason to believe that there are people being harbored in this house.
    - The neighbors are coming and crowding the guards frustrated at their actions they think are unreasonable.
- Elidee still trapped inside sees the kids being asked questions by the guards.
    - The guards don't seem to be threatening the kids.
    - Elidee then sneaks out of the room
- Urist sees the crowd getting bigger and louder towards the guards.




- Elidee turns back to human form. They decide to go to the safehouse and see if the soldiers are there.
    - It seems that the guards were just standing outside. Probably waiting for a leader.
    - They decide to go towards the library instead.

- The guards are a couple of doors down the street of the library. The guards are raiding a house there. They're not waiting for the leader.
    - The house is a single floor home. There might be people inside.
    - A crowd of citizens is also starting to form around the street.
    - Elidee [[Checks#Perception]] 22. Notices 
        - A woman screams: "You've already taken him once!" 
        - It seems that the guards want to take the dwarf librarian who appears to live inside the house.

> We will leave this here!
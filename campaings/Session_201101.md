# Elidee and Rircin at the Fox and Goose

This is a continuation of [Back to Elidee and Rircin](Session_201025.md#Back%20to%20Elidee%20and%20Rircin)

The crew conforms of [Elidee](../characters/Elidee.md) and [Rircin](../characters/Rircin.md)

> They arrived to [Fox and Goose](../locations/Fox%20and%20Goose.md)
> They let them stay the night and wait for [Coraline](../characters/Coraline.md) to arrive.
> She should've arrived.
> It sounds like something happened to Elidee's mother in the forest of [Forestera](../locations/Forestera.md)

The party wakes up from a long rest.

## Breakfast

- Party goes downstairs.
    - The ambiance in the tavern is slow. People are just waking up
    - So far, the people in the tavern are familiar from last night.
        - A gnome that sat alone last night appears to be sitting with some dragonborns.
- Elidee would like to get some breakfast
    - There's bread, bacon and coffee avaialable.
    - Elidee ponders on the possibility of drink beer early in the morning.
        - Elidee gets a beer
    - Rircin is fine with Coffee
- The crew sits on a corner overlooking the entrance. They want to see whoever comes in.
	- ![](../attachments/Pasted%20image%2020201101112517.png)
- A [well dressed knight](../characters/Alistar.md) has come down the stairs and makes his way to the bar.
    - Elidee [[Checks#Perception]] 22 to overhear what he says
        - They manage to overhear the words: "spy", "accident" and "traitor"
    - Rircin [[Checks#Perception]] 8 to overhear. Fails
- The knight sits down on another corner and can directly see Rircin and Elidee.
    - The knight appears to also be awaiting and looking at the door
- Elidee suspects the knight to be expecting trouble
    - Rircing attempts to sneakily go to the coffee pot for a refill and strike up a conversation.
    - Elidee goes outside to see if anyone is there.

## Checking on the knight

- Rircin starts a conversation with the knight
    - Rircin asks if they're coming or going
    - Rircin [[Checks#Insight]] 20
        - The knight appears to be attentive to what Rircin says.
        - The knight is paying more attention to the door than at Rircin, though.
    - The knight says that he enjoys the tavern
    - Rircin would like to spar for a bit with the knight.
        - The knight thinks that they have very differently skilled and wouldn't be a fair fight.
        - Rircin insists on sparing
        - The knight **agrees to spar with Rircin** to do it before the midday meal.
    - The knight points out that the person Rircin is escorting has left the tavern.
- The coat of arms of the knight is similar to the son [Lady Seva](../characters/Lady%20Seva)

- Elidee is assuming that the nicest horse she saw last night is owned by the knight.
    - Elidee finds the horse in the stable.
    - Elidee got an apple from the tavern before leaving
    - Elidee #cast speaking with animals
- Elidee begins to talk with the horse
    - The horse calls itself Yack.
    - The horse is from the south
    - The rider of the horse lives in a castle
    - The horse says that they come from a different castle at the North-West. Before that the horse says they came from a forest.
    - Elidee asks the horse if they know the name of the Forest.
        - The horse says that it's called [Forestera](../locations/Forestera.md)
    - The horse says that they were running away from Forestera from beings that look  just like Elidee.
        - The horse thinks they **saw their people 3 moons ago**
    - The horse doesn't know what their next destination is.
- Elidee asks Rircin's [Horse](../characters/Rircin.md#Horse) what their preferred name is?
    - The horse says that Rircin is the nicest owner he's had
    - The horse says that that his name is whatever Rircin decides


## A gnome arrives

A donkey with a gnome arrives to the Fox and Goose

- The gnome goes inside calling for help
- The gnome comes out *pulling* the [well dressed knight](../characters/Alistar.md) out of the tavern.
- The gnome and donkey are ready to go as the knight is preparing the mount of his horse.
    - Rircin approaches the knight
        - The knight is now declining the sparring, as this is the person he is waiting

- Elidee recognizes that the gnome is associated to her tribe.
    - Elidee [[Checks#Perception]] 19
    - Elidee doesn't know the gnome, but the clothes are certainly from her tribe.
    - Elidee #cast paralysis
        - The gnome is held in place
        - The gnome says that they're not impersonating anyone
        - The gnome says that the **consequences for a person like Elidee are greater than for the gnome** if they're not released.
            - Elidee offers releasing the gnome if they let them come over and help
    - Elidee mounts behind the gnome on the donkey
- Rircin goes to prepare his horse in the stable

## Riding out

The donkey, is at the front. The knight second and Rircin coming at the back

- Elidee #cast Pass without Trace. This prevents the crew for being tracked.
- The direction of the ride appears to go in the direction of the [City of the South](../locations/City%20of%20the%20South.md)
- Rircin, at the back [[Checks#Perception]] 11
    - They see vagely horses in the distance behind him. They appear to be slowing down.
- The gnome says to Elidee that they were a distraction and that **he got made**
    - Elidee says that she was waiting for her mom
    - The gnome says that **faulty information** is likley coming from the City of the South.
    - Elidee wonders if the tavern keeper was aware of this and might be
    - Elidee [[Checks#Insight]] 1
        - The gnome is panicked
        - The gnome says that the inn-keeper is in no danger, as the landlord doesn't know any information that might put him in danger.
    - Elidee slows down the donkey because she wants the inn-keeper to be safe.
- Rircin catches up with the front of the ride
    - Elidee is afraid for the inn-keeper
    - Rircin says that there appear to be 3 horses in the back. Rircin expects to be even 3 more horses behind
        - Rircin convinces Elidee that they wouldn't be able to save the inn-keeper if he was really in danger.
    - Elidee accepts this and lets the crew **continue at full speed**

- Rircin is now talking with the knight
    - The knight is named "Alister"
    - The knight belives to have allies in the city of the south once they arrive.
    - The knight says that:
        - There is an agent of a foreign government operating within their midst
        - In the City of the South they expect to be able to aprehend them
        - They are operating at the highest levels of the nobility.
        - The knight asks if they've heard of [Iman](../characters/Iman.md)?
    - Rircin pretending to know of Iman, asks for more information
        - The knight says that they're pretty busy to discuss about it as they're being chased.

- Elidee points to the knight and gnome that they're being covered by the Pass without Trace.

#unresolved-thread

# Xairan

After negotiating with Vosha, with time left before starting his new job [Xairan](../characters/Xairan.md) goes to the small library

## Library

The library is attended by an elven person.
The bookcases seem to be very narrow, almost like the library is good for storing books and not for people

- Xairan [[Checks#Investigation]] 15 looking for information about prophecies.
    - Finds information about boring groups of 
    - There are groups about the rise of dragons
    - A book titled "The art of storytelling"
        - A person scribbled on the book the words [Navin](../characters/Navin.md) **restore the rightful to power**
        - The book is written by a person from [Emeland](../locations/Emeland.md)
        - The book appears to have underlining bits about distribution of power and storytelling with sections about different races.
- Xairan would like to know who else might have borrowed the book
    - The book looks that it **doesn't belong to the library**. It doesn't have stamps from the library
    - The book is sufficiently small, and Xairan claims to know be of his property.
- Xairan sees the shelves, wondering if someone left the book there on purpose
    - Xairan [[Checks#Investigation]] 15
    - Based on the dust pattern, it looks like it was placed along side the rest of the bookshelf, so it doesn't look to have been recently picked up or had special treatment than the rest of the shelf.
- Some book information:
    - The book appears to have been payed for by a man named [[Chinor]] 
    - The book was written by the scribe Rostan
    - The was ?? by Yulduz

## Tavern

Xairan goes to the tavern on time to his job.

- Xairan recieves instructions on how much to charge for some drinks, but not all.
- Xairan compliments the female patron of the tavern
    - She thinks is suitable clothes for *the occasion*. There's a party hosted by some friends of her in which she plans to **meet a Captain**.
    - She thinks is not the soldiers from the guard, but the ones who fight outside of the country. The ones who will **actually protect them**.
- Xairan states that he would like to thank those soliders fro their service.
    - The keeper will see what she can do.	

- A group of 4 elves arrive in reasonable attire. They seem to be upper-middle class
    - They order beers. 
    - Xairan asks them to pay upfront.
    - The group pays
- A group of 4 men arrive
    - They wonder who Xairan is, as this person is new
    - They want to order their regular *town special*
    - They put the money on the bar. 
    - Xairan gives them Bailys
- The group strikes a conversation with Xairan. 
    - Asking him about how he like the city
    - Xairan talks about finding good jobs easy given his talents.
    - Xairan says that some guards mistook him recently.
    - The group thinks that guards used to be better when **they were guards**.
        - The group then talks about a story about chasing an assasin through rooftops and eventually fell on a gutter.
        - The group sounds like they were cartoonishly busy guards
    - The group thinks that the previuos governor's guard was nicer to deal with.
- Xairan [[Checks#Insight]] 6
    - Xairan notices that their experience is sincere
- Xairan asks more about the new guards the group talks about
    - There are 2 groups. The city guards have to be more serious now. The outer guards are generally out of town, which is part of why they don't like them.
    - There's some **unrest toward people who have lived here for long**. 
- Xairan asks about the old governor.
    - The old governor was [Escarador](../characters/Escarador.md) he was entrusted by the King to rule for the city, for as long as his children were old enough to take over the city.


Xairan goes back to safe house after closing the bar

# House goes shopping
[Amryn](../characters/Amryn.md), [Xairan](../characters/Xairan.md) and [Urist](../characters/Urist.md) are back on the safe house.

- Amryn believes that her long term friend [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) might have ratted her out.
- Xairan might have some interesting information regarding her friend.
    - A book found with content about prophecies about divine ruling leaders.
    - Xairan thinks that the current governor might be planning for a **total domination**
    - Based on the descriptions from the librarian associates of Amryn's friend who went to the library are [Oba](../characters/Oba.md) the person who suggested that [Coraline](../characters/Coraline.md) was alive.
- Amryn isn't surprised from her friend's interests in politics.
- Xairan thinks that Coraline might also be involved in some conspiracies.
- It would be nice for safety to contact Elidee for protection, but the party doesn't have a means to communicate given their distance.

The party takes a long rest.

## Next morning
The blast furnace is being installed.

- Amryn thinks of going shopping after having breakfast
- Urist will make breakfast
    - Amryn doesn't have a preference for breakfast
- The crew is thinking of what the best illusion could help Amryn desgise herself
    - Xairan suggests changing race
    - Amryn **disguises as a half-elf**
    - Norman is now **disguised as a dwarf**
- The crew thinks of going to Pikes, a high-end store for clothing
    - Urist doesn't believe they will have gnome-sized suits.
- The store clerk says that they can provide clothes for all of the crew.
- Amryn would like something blue
    - Xairan thinks that blue is _his thing_
- The clerk says that popular colors in high society is to use colors of their coat of arms. The king wants to have colors from all the parties
    - Amryn thinks that green might be a good color
    - Urist now feels territorial about _his color_
- The clothes for Amryn will be ready tomorrow

- The tailor now approaches the men. They get offered coat tails.
    - Xairan would like a 3 piece suit

- The whole order will be ready tomorrow and cost 30 gold pieces.

## Dwarven wear

The crew  now goes to get some nice dwarf formal clothes

- Urist would like to know of aesthetic norms in the city
    - The clerk looks at the description of maroon and green suit.
- Urist would like to include his pickaxe in his suit, arguing that in his tribe, it is a cultural part of the attire.
    - The pickaxe is large enough to go on the back.
        - It would require a harness around the back
- Norman got fitted 


## Library

The party is in look for [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md). Xandar suggests going to the small Library. 

Today, the library appears to be more messy than yesterday. The state of the shelves is in disarray.

![](../attachments/Pasted%20image%2020201101153831.png)

- Xairan and Amryn #cast Blade Ward to potentially protect themselves.
- First, they go to find the homosexual dwarf librarian
    - The librarian is no longer there
    - A destroyed book is found around them. 
        - The spines of books were torn and pages are on the floor
- Looking at the ledger of the library, there's pages of who borrowed the book of "art of storytelling"
    - Despite the book not being part of the library (according to Xairan), it was in the ledger
    - Amryn states that [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) was using an alias to borrow that same book
    - Xairan states that the former governor [Escarador](../characters/Escarador.md) of the [City of the South](../locations/City%20of%20the%20South.md) was one of the general borrowers of it.
        - Amryn thinks that maybe the book could be used to send messages.
- Urist [[Checks#Investigation]] 6 to check if there's someone still inside the library
    - There is lack of light in the library as a whole
    - As Urist gets walks along the messed up book cases, a groan is heard.
    - Covered in bruises, Urist finds a gnome among books on the floor
        - She isn't covered in blood.
    - She seems relieved that _they're gone_.
- She says that she was inside the library looking at the community library.
    - She says that it was guards who decided to ransack the library.
        - She is friends with the Librarian
    - Given that the party isn't wearing the [Blue and Green](../misc/Blue%20and%20Green.md) colors, the gnome believes that they're not part of the guards.
    - Urist states that they haven't seen the librarian yet
    - Urist [[Checks#Investigation]] 3: Nobody appears to be under the rubble.

- The rest of the party is also going to investigate
    - Amryn [[Checks#Investigation]] : 8
        - The library is chaotic with books on the floor.
    - Xairan [[Checks#Investigation]] with advantage: 9
        - The book case that contained the "art of ..." is untouched compared to the next 
        - As he starts moving the books in other cases a "click" sound is heard.
            - Xairan yells out to the party about a potential secret door
            - The dwarf lady over-heard that and is also startled.
        - Smoke starts appearing behind the book he just pressed
        - Xairan sees pieces of glass and a spring behind the book
            - The glass **just released some gas** it appears to be a trap
        - Xairan [[Checks#Investigation]] 14: The gas appears to be acid
            - The flask it was in was about 15 ml 
            - The books around the flask start to be damaged
                - Xairan [[Checks#Dexterity]] 15: Starts to save books from the shelf
                - Xairan cast ray of frost to slow down the acid
                - Half of the message is still readable
                	- OND help Navin
                	- Blessed us for ILIN
 	- Urist gets the dwarf out of the library
        - Amryn asks her about what happened
            - Edmund (the librarian dwarf) and the guards had an altercation
            - Edmund hasn't been found
            - They should take this information to the **City Guard and not the Governor guard**.
            - The guards were interested in the shelf where Xairan is
            - Xairan said that he managed to freeze the acid which had some message
            - Xairan said that the expected book wasn't in the bookcase either,
        - Fs Freya is the name of the gnome
    - Amryn will stay back to clean up the library, while also having time to disguise away from Ms Freya
        - Amryn puts the loose pages arond the librarian desk
        - Amryn [[Checks#Investigation]] 7: searching for more stuff
            - Amryn founds 1 GP
    - Amryn #cast disguise person to go back to her previous disguise.
    - Amryn [[Checks#Perception]] 14 to see if someone looking at her direction and the library.
        - Amryn notices that there's not a lot of Blue and Green around the streets.
        - Amryn [[Checks#Perception]] 11: She isn't being followed


Xairan and Urist hear MS Freya talk about her hobbies and favorite food.

- Xairan and Urist cautioslly walk back home
    - Urist [[Checks#Perception]] 18: The crew isn't being followed

As they get close to home, they see the half-ling at the door.

![](../attachments/Pasted%20image%2020201101164556.png)

## Chemistry

- After reheating the acid the new message revealed
	- "Found Help Navin The Power of King Has Blessed Us Who Value Might of Eviellan"
- They discovered that it is hydrocloric acid.

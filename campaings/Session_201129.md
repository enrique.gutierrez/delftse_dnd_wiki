# Last session
- It is the evening
- Elidee saw Xairan
- The party heard from an owl 🦉 emphused by a message from Elidee
> Xairan is being taken to the main city watchtower, near the Bored Goats'Inn, by 4 Governor's guards. Don't know why. I am following...
![](../attachments/Pasted%20image%2020201129132004.png)
- The group is surprised 

# Today's session
- Amrynn wonders what Xairan was doing
    - Norman doesn't know
- Amrynn would like to just send a package of lockpicks
    - Urist thinks that lockpicks aren't enough
    - Norman would like to break him out
    - Rircin reminds the party that the gala is tomorrow
        - Amrynn reminds the party that they need to pick their clothes.
- Rircin suggests checking the tower to see if Xairan can break out on his own
    - Amrynn thinks that her or Elidee could send him lockpicks
    - Rircin [[Checks#History]]: 18
        - The party knows that the jails would be underground
    - Amrynn think they could cast some _sleep_ to some guards. 
- Rircin and Urist think of picking up the **clothes from the tailors**
> The party has 26 GP left from the 200GP given to them by [Vosha](../characters/Vosha.md).
- Amrynn plans to **drop the lockpicks**. 
    - Norman wants to join Amrynn
    - Amrynn threatens Norman if he does something stupid by having 11 fingers at the end of the night.

## Xairan escorted and Elidee following

> Xairan is being walked by the guards, has not noticed if Elidee is nearby. Xairan can see that the bridges have guards around.

- Xairan [[Checks#Perception]] 3: Xairan can't count how many guards are around the busy street.
    - Xairan doubts whether fighting the guards will work.
    - Xairan will continue walking along the guards
- Elidee continues to observe where Xairan is going
    - Elidee [[Checks#Stealth]] 19. She manages to follow Xairan without being noticed by the guards.

...

- As the guards are crossing the bridge into the tower
    - Xairan [[Checks#Perception]] 13
    - Xairan sees the bridge and notices that the water below could be deep enough to hide 
    - The weather is grey and gloomy 
- There's a guard on both sides of Xairan so it won't be easy for him to get away

...

- Xairan chooses to keep walking, unable to come up with a scape plan
- The guards reach the watchtower
    - The ground floor
        - ![](../attachments/Pasted%20image%2020201129141128.png)
    - First underground floor
        - ![](../attachments/Pasted%20image%2020201129141101.png)
    - The walls appear to be made out of stone. Underground there's even ground around
- Xairan yells at the guards to know where he's being taken
    - Xairan notices that there's echoing in the cellar
    - No one reacts to his cries for attention inside the cellar.
- Xairan can see people in the jail cells but **can't recognize anyone** [[Checks#Perception]] 8.
- Xairan is lead to an interrogation room
    - Xairan [[Checks#Strength]] 6. The guards **force xairan to sit down**  on the chair.

...

- Elidee notices that Xairan was lead into the tower
    - Elidee, takes a hiding location where she can spot for the tower
    - Elidee [[Checks#Perception]] 15
        - Elidee can see [Alistar](../characters/Alistar.md) and the Gnome walking by the castle next to the watchtower
- Elidee turns to a weasel.
    - Elidee [[Checks#Perception]] 12
        - Elidee smells that Xairan is in the cellar. Also notices the smell of [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md), as well as some meat


## Rircin and Urist go to tailor
- As the people are walking through the streets
    - Rircin [[Checks#Perception]] 23. Notices that the house is being watched by governor's guards. They're clearly **watching you** and not being sneaky about it. There's 4 in total. 2 on each opposing corner.
    - As Rircin and Urist walk pass the guards, they look at the guards, and they look back, but **they don't follow**, they keep watching at the house
- The reach Pikes
    - The clerk says: Your order isn't ready, **until tomorrow**
    - Rircin: how about my friends?
    - Clerk: Oh, the dark green for the elf and the blue one are ready.They've already been payed.
    - *Rircin leaves without tipping*

...

- Rircin and Urist head to the other shop
    - Urist is recognized by clerk
    - The clerk has the order ready
- Rircin thinks that it would be wise to arrive home and drop the clothes first and the 
        

## Amrynn and Norman go to the watchtower

- Amrynn is **disguised as a human**.
- Given that Amrynn has been in the city, she knows where Elidee and Xairan would be.
- Amrynn and Norman leave through the backdoor
    - Amrynn [[Checks#Perception]] 16
    - Norman [[Checks#Perception]] 21
    - There are city watch around the streets but **noone is watching the house**
- There seem to be more guards in the city than yesterday.
- They reach the Goat's Inn, but can't see Elidee
    - Norman thinks that Elidee should be disguised.

---

- Amrynn and Norman have crossed the bridge
    - Norman hasn't noticed Xairan or Elidee yet, only **people they can stab**
    - Given that there's no way to *cast message* safely to contact Elidee, they might have to go into the tower
- Norman and Amrynn think of pretending Xairan is their cousin and **pay bail** for him.
    - Norman states that he has *charm spell*  which can be used to talk the guards
    - Norman #cast disguise and turns to human-like

> They go into the watchtower and hope for the best at talking to the guards. There's a poster in the walls for Amrynn's face, which means she can't loose her disguise while inside.

## Inside the watchtower
- Elidee can't see Xairan in any of the cells, so he must be somewhere else
    - Elidee smells and thinks that Xairan is inside a room but not sure where
- Xairan is being interrogated
    - Xairan sitcks to the story that the trap was activated "somehow". [[Checks#Deception]] 14
    - The guards don't believe that the trap would've burn the library as quickly as it did. Would you like to revise your statement?
    - Xairan [[Checks#Insight]] 22.
        - The guards are not mean to Xairan. They're not trying to for him to confess. They want to confirm that the story makes sense with what it happened
    - Xairan thinks that there might've set up more traps. [[Checks#Deception]] 12.
        - The guards couldn't find any traps. The guards **want to know what reason he has for being in the city**
    - Xairan argues that he's a traveller with a group.
        - Xairan says that he doesn't know much of the groups' backstories.
        - Xairan says he likes the library because he likes reading books
        - Xairan says he likes small libraries because of their folk tales as opposed to large city libraries.
        - Xairan says he's never heard the name of [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md). But knows of *an Emlanor*
        - The guards know that he has been a guest at Xairan's place
        - The guards plan to keep Xairan in the jail until he answers the questions they want to know.
    - The guards move to escort Xairan and he **prepares to cast a spell**
- Weasel Elidee is looking for some way to find Xairan
    - Elidee [[Checks#Investigation]] 19. She notices that the room 
    - Elidee #cast minor ilution for an alarming noise
        - One of the elves that were with Xairan goes outside to investigate
- One floor above, Amrynn and Norman
    - Amrynn [[Checks#Perception]] 12, Norman: 10
        - They don't see or hear anyone
    - They decide to go left in the room
        - It appears to be a rest room for guards
        - Norman [[Checks#Sleighthand]] 1: Norman tries to grab one card to steal, and just drops all of them. After sloppily picking the cards, they go to the other door
    - They knock on the right door
        - Three guards are on the other side. 
            - Amrynn claims to be looking for the administrator of the tower.
            - The guards are dismissed and sent back to their posts
        - The boss asks them what they're looking for
            - Norman asks about their cousin Xairan
            - The boss says that he had claimed is new to town. Have you visited him to the Safe House
            - Amryn [[Checks#Deception]] 13, Norman 19
                - We haven't seen his travel companions
            - The boss believes it
        - Amrynn asks why he's being taken
            - He is a known associate of someone they're looking for
            - He's also involved with a fire at a library
            - Amrynn argues that he's clumsy and tends to play with chemicals.
                - The boss says that he is happy they came, and asks them to wait.

> Outside of the castle, Urist and Rircin have gone to the Bar and gone for a beer to wait for people if they see them.

- Urist and Rircin will stay talking in case something happens

> Back in the tower, we go back to Elidee

- Elidee turns into a spider and goes under the door to Xairans room
- Xairan still sitting down, asks the remaining guard what they're intending to do to him
    - The guard states that they need to interrogate him and want to confiscate his gear.
    - Xairan cast [[Blade Ward]]

- After a discussion in which Xairan refuses to cooperate, a **whistle is blown**
    - Amrynn and Norman hear the whistle.
    - The guards in the recreation room spring past them and go downstairs
    - Amrynn and Norman think of going through the door and **go help** their cousin.

- With the commotion of Xairan, Amrynn cries for her cousin telling him to behave
    - After a back-and-forth, with the guards, Xairan talks to the guards boss
        - Xairan claims that he's been mistreated
        - The boss evaluates the behavior of his guards and shows Xairan he's been treated properly
    - Amrynn tells the guard boss that Xairan is misbehaving, but that she can vouch for him
        - Amrynn says that she's lives in a simple home in **Penecroft Alley**
        - The boss offers Xairan to pay back his dues by helping fix the library, given how the family seems to live in a poor neighborhood
            - Xairan is **released**
- Urist and Rircin still drinking [[Checks#Perception]] 12
    - They see someone leaving the tower, but can't tell who it was.
    - They decide to follow the people
        - They find Xairan and **new-found cousins**

> The party is going back home. They now approach the board goats Inn

- amrynn [[Checks#Perception]] 25
    - the party is not being followed
    - Norman and Amrynn split from the group before they reach home to make sure they all go back the way they came in
    - Amrynn [[Checks#Stealth]] 26: She hides in the shadows and makes it home
    - Norman [[Checks#Stealth]] 3: He stumbles and falls, and noisily makes it home. Luckily noone was looking at the back door of the home


## Elidee stays behind

- Elidee spider stays back for a bit as a spider to listen in on the guards.
    - "That was pretty useless. We know he did it. But we might need to get inside their house at some point. We expect enough high profile people in the ball to provide their own security."
    - "We don't expect many guards at the ball, as we don't want to create the appearance that it is dangerous location"
    - A Lord comes in
        - "I want you at the ball - says to the boss"
        - "They expect the ball to be going fine"
        - "They expect some commotion in the city. They don't trust the city watch to react to the situation accordingly."
- Elidee spider follows after the lord
    - The lord is visibly **a half-elf**
    - The lord reaches a house in a nice-rich neighborhood
        - Noone greets the lord, so she can't determine a name
        - No plaques are visible in the home
    - Elidee turns back into regular form
    - Elidee tries to memorize the details and features of the home
  
## Back in safe house
  
- Elidee tells the crew about what she learned by staying back in the tower
    - They expect some issues in the city but not during the ball
- Elidee plans to hide Amrynn's ears so she doesn't have to wear a disguise so long
- Xairan says that he expects the following people in the ball: Edwinem, Evan, Ardian, Alastar

> The party will decide who will go to the ball and who will go to the city.
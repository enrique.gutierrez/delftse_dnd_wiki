# Session
## Elidee, Rircin and Urist on the town

> Streets are not very crowded. They're going to the Library

- Rircin: First step is to check if they'll let us in to the library. Also, with the latest fire they might not think
- Elidee: We're looking for info on Navin, dragon eggs and sourcerer bloodlines

> The party reaches a 2 story library. This is the *Royal Library*. There's an atrium and a desk in the center. A stern lady is occupying the book

- Rircin: Can we browse?
- Lady: We need to know someone who can vouch for you. We don't just lend to unknown folks anymore.
- Elidee: Maybe we should use [[Sir Alister]]?
- Rircin is checking the boards [[Checks#Perception]] 19
    - They 

> The group reaches the house of Sir Allister. They're welcomed by a door-man who's wondering why they're there.
> Instead of Sir Allister, the gnome comes out

- Gnome: Sir Allister is very busy. He's running interferance for you all. You need to get better at this.
- Rircin: We're working on it. Are you a member of the library, by any chance?
- Gnome: No. I don't read much.
- Rircin: We'll be on our way then.
- Gnome: Last night was a disaster **exercise caution next time**
- Urist: Why did the governor guards were all outside the castle during the event?
- Gnome: Though motives don't make the most sense, he wanted to show safety to the city. 
    - The governor is hoping to re-establish a forgotten line with our neighbor country in the south.
    - Urist: This is what we want to learn more of in the library
    - Please **come back tomorrow**

## Norman, Xairan and Amryn 

> Some city guards seem to be happy to be *taking control back* form the governor guards.
> Xairan is awakening

> Nell is knocking at the door. Looking frazzled and makes herself invited to the home. She sits down on a barrel

- Nell: I'm concerned! I heard some people like you are wanted! What is going on?
- Xairan: Yes. I was at a library and I triggered a trap It wasn't me. Do you think I cou;d've done that?
- Nell: I don't know you that much. I don't know if I can trust you.
- Xiaran: Some guards are framing me and a friend. We need to sort it out with her acquaintance.
- Nell: Oh! So you're not just in trouble with the library?! Look I can't trust a person like you to come again tomorrow. If these rumours are true, you are not welcomed tomorrow.

> Nell hands money to Xairan 


## Party returns

> At the moment, The entire party arrives the u

- Nell: Okay. I **want to see you in action tomorrow** Clean up your act.

> Nell leaves.

## Addie arrives

> [[Addie]] arrives and demands Norman to come with her and the guards.

- Rircin: He's not here yet (*lie*)

> Addie leaves

- Rircin: Norman we need you here! They need you at the castle tomorrow!
- Norman: What? Why?
- Rircin: Just go there and apologize. Worst case, you can try a **trial by combat**
- 

# Session
> There's some book keeping.
> The party is traveling out of the City of the South

![](../attachments/Pasted%20image%2020210328134602.png)

## Learning languages

> It is decided that to learn languages you need **inteligence**
> D20 - roll to learn
> 
> | range | roll |
> |----|---|
> | 1-5| 1d4  | 
> | 6-10| 1d6 | 
> | 11-15| 1d8 | 
> | 16-19|1d10 | 
> | 20| 1d12 | 

- A: I want to learn draconic from Xairan!
    - A: Roll 4 intelligence
- X: I want to learn thieves cant from Amryn!
    - X: Roll 6 intelligence

> The party before reaching the toll they decide to go hunting before making camp.

## Going hunting

> Xairan and Urist go hunting

- Urist: We're going for big game!
- Xairan: I just need food for my baby dragon that will soon hatch

> Xairan is not as experienced looking for hunt, however Urist notices some tracks to follow. 

- Urist: I will follow the **goat tracks**. 

> There are marks of goats with horns. They seem to have a strong smell. The goat is now in the sight of the party

- Xairan: I want to try sneaking.
- X & U: [[Checks#Stealth]] 7 and 8

> The goat moves far but calmly

- X & U: [[Checks#Attack]] with their crossbows
    - They hit the goat for 9 damage
    - The goat dies (only had 4 HP)

> There are some rabbits nearby

- X: I also want to get some rabbits
    - U: I don't think we want to attack them from afar. We either need a snare or try to catch them.
    - X: I'll try to catch them

- U: [[Checks#Survival]]: 5

> Their lack of prepardness, leads to a bad attempt of making a snare

- X: [[Checks#Dexterity]] with disadvantage: 4

> They do not catch the rabbit

- X: Maybe we should try also going for the badgers!
    - U: I will block one of the holes
    - X: I will make a pressure wave with Thunderclap at a second hole

> A third hole is open. In a chance of panic, the badger comes out via the hole near Urist.

- U: I will punch it!
- X: I attack with freezing!


> The party returns to the camp with a frozen badger and a dead goat

## Back at camp
> A tent has been pitched.
> Elidee is an introspective mood. Taken to sitting in the tent by herself.

- A: Is everything okay?
    - E: I'm okay. I want to have some quiet time.
- X: I want to make the dragon hatch!
    - X: I will put the egg into a fire with the tongs.

> A tiny pseudo-dragon with tiny wings unrolls. blue and scaley
> It is communicating telepatically in series of images. 

- D: Sends image of fire, lightnining bolt and water (river)

> Xairan is now being more gentle to the dragon than anything or anyone else in the campaign.
> Elidee is collecting sticks and stuff near the river on her own
> Urist is making dinner.

## Back on road

> The party wakes up and goes back to the road.
> They pass the toll without paying anything.
> They pass a large castle that overlooks both rivers
> There's signs of commercial actiivities on the sides of the rivers

> The party as they travel they pass next to a guard next to the bridge in between the large castle


- The guard: I need you to pay 3 GPs to pay the toll!
    - X: We have tokens from Sir Allistar. We're on a buisness trip
    - E: I have the tokens
- X: I need to hide the dragon!
    - X: You can stay warm inside my coat
    - A: You can have some meat!
        - D: Gets on Amrynns shoulder, and promptly communicates a **sky image**

> Dragon is being lifted with Mage Hand up to the sky
> The dragon is happy and projcts images of joy.
> It is unable to fly yet.

- X: I'm glad you enjoyed flying, but we need to hide you!
    - D: Projects more meat
    - X: I open my coat, put the meat in my pocket

> The dragon goes into the pocket. 
> The party passes the toll without anyone making a scene.

## Into the dense forest
> After passing the toll, and crossing the river the party ventures into a thicker part of the forest

- R: I'm going to dismount. The horse and I can't fit through the woods.

![](../attachments/Pasted%20image%2020210328163623.png)

> The party slowly manages to **run away from the conflict**.

- Urist notices some shiny things near the grass and goes to get them
    - Urist finds 2 moon stones.

> The dragon wants more meat

- X: I feed the dragon more meat

> The party plays with the dragon
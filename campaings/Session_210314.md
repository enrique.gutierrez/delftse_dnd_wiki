# Session

> Rircin and Elidee are returning to the safe house to see Xairan and Amryn speaking in Elvish

## Dealing with Emlanor

> Amrynn explains the adventure of last time how they came to find Emlanor in prison.

- Emlanor after the battle of vorgota he decided to become a spy.
    - A free agent kind of spy
    - There was a group abducting children. 
        - Lord Samuel was given this info but didn't care about it
    - He was blackmailed to become a spy for the Knights of Dredor
- Elidee is not happy with the possibility of children being in danger
    - Amrynn: This happened 40 years ago
    - Elidee: We need to know more

> The party goes upstairs to find him in bed with Norman and bring him downstairs after asking him to **put on pants**.

- Amrynn doesn't want to talk to Emlanor
    - Xairan tries to calm down Amrynn
    - Amrynn doesn't want to deal with either men right now. She [[Checks#Attack]] unarmed strike but fails.

> Elidee and Rircin pull Emlanor to the kitchen to talk in private

- Rircin: Can you tell us more about the abducted children?
    - E: They were returned to their parents. I tried to get evidence to make Samuel to believe me.
    - R: Why were they abducted?
    - E: It was a recurring thing to try to expand their influence.

> Amryn interrupts the conversation. She wants him to talk about Dredor

- R: Look, I don't think you're cut out to be a good diplomat
- El: How large is Dredor's influence?
    - E: They're trying to expand to the south
- ...
- X: What if we send you back to Dredor?
    - Emlanor: Don't!
- A: What if we hand it over to Sir Allistar or the Catlyn?
    - E: I would prefer it to go to the highest place? How well do you trust Catlyn?
        - X: They might not be very competent, but **aren't we the same**?
    - R: Is there anything going on between you two?
        - X: things are okay.
        - A: I'm frustrated at his childish behavior towards Emlanor. He is more injured than when he was in prison and with trauma.
- X: If we hand him over to Dredor we can become second agents!
    - R: Because that worked so well to Emlanor, right?

> It is revealed that there's a second prisoner taken by Amryn in the shed


## Dealing with prisoner

> Elidee goes to the shed to see if the 

- Elidee casts "Healing Word"
    - 9 points of healing
- [Yak Withawye](../characters/npc/Yak%20Withawye.md) confesses that he's only less legal muscle for the governor's guard
    - Some of his work has been to intimidate young contras for the governor and kidnap [Oba](../characters/Oba.md)'s husband

> Elidee breaks from the party

- E: Sir Alistar gave us a letter of immunity. He wants the party to go to the **Grey Hill** to go to the camp where there's some 
    - R: I do want to **see Lord Samuel**
    - X: I would also like to go the **White Hills** after

![](../attachments/Pasted%20image%2020210314150125.png)


## Meeting Catlyn

> Riricin will stay behind to watch over Emlanor
> The rest of the party to meet Catlyn. They will take Yak with them. He's been charmed to avoid causing a scene.

- Sir Allistar steps in. Somewhat displeased to see the party
    - Interesting that you want to hand him over to me? He was already in somebody's custody.
    - I thought I had given you something to do. I hope that you can get the things I asked of you within the next few days.
    - Why is this man (Yak) out of prison? It took us some political effort to get the governor to have someone accountable for the actions of the governor's proxies.
    - Stop getting into **messes that you don't understand**.
    - You need to get to the Grey Hill as soon as possible.
    - Emlanor was in prison, but **not because of us**
- SA: What will you do with Emlanor?
    - A: He'll have to turn himself in. He's also has people from Dredor looking for him
- X: Do you think we could hand him over to Dredor? Maybe get some favors from them?
    - SA: Wouldn't that also lead him to his death? I do think it's an interesting idea but I need to trust you first. So please show me that you're capable of fulfilling the tasks that I 

## Nightmares

> The party goes to sleep and they have a collective nightmare [One-shot](Session_210228.md#One-shot)


## Going shopping

> The content of the [Shopping](Session_210228.md#Shopping) is what happened

## Morning after

> Sir Alistar comes in the morning to pick up [Emlanor Stormwind](../characters/Emlanor%20Stormwind.md) from captivity of the Safehouse.

## Leaving the city

> The party is now on it's way out of the city.
> They pass by a toll 
# 2-May-21

## Recap
> - Dragon is still alive
> - [[Norman]] wants to die
> - They fought plants
> - They found some refugees
> - [[Rircin]] woke up and **found [Elidee](../characters/Elidee.md) gone**

## Session

### Elidee is gone
> It is early in the morning. The light is starting to shine. Rircin is confused

- Rircin is sharpening a sword and reading a letter
    - Amrynn sees Rircin and asks him what is happening
    - Rircin: Elidee is gone. So are the refugees.

> The refugees were on their way to a village near mountains in the north of the map. It's presumed that Elidee took the refugees.

- Amrynn: Isn't she in trouble?
    - Rircin: The letter states that she **doesn't see us as a good role model**
 
 > Xairan wakes up

- Xairan: What is going on? Where are the people? 
    - Rircin: She left a letter.
    - Xairan: Can I see the letter? Did she actually write it?
    - Rircin: The letter was personally addressed to me

> Rircin seems to be **very protective of the letter**. He chooses reluctantly to show a letter.

- Xairan: It's a long letter. I can't read this much!
    - Amrynn: You can read no?
    - Urist: No wonder she left, you can't be bothered.

[Dear_Rircin_-_final_version](../attachments/Dear_Rircin_-_final_version.pdf)

> Rircin will be handing objects that Elidee left.
> For Amryn a painted picture of Amryn and the old familiy looking down to the new familiy.

- Amryn: I haven't had something so nice in a while

> Norman recieves a sheet
- Norman: "Nice"

> Urist, half awake recieves a journal with embossed lettering with pictures of the adventures that Urist and Elidee had together, like the mice, the cannonball and the dancing ball

- Urist: How and when did she make this?! she should've opened a store. 

> Xairan gets a wooden collar for the dragon. There's an egg-shaped looking gem about the size of a thumb. It is similar to the typical tribe of Elidee. 

- Xairan: She's a sweet kid. I would've wanted to get her something for her birthday
    - Amryn: Maybe we can send a gift
    - [ ] Amryn to send a gift to the [[Forestera]] for Elidee


### Deciding on Camp

> The party is looking at knowing how to infiltrate the camp

- Amryn: How's the dragon?
    - Dragon 🐟
- Amryn: Let's train you a bit!
- Xairan: I will teach you!

> Dragon rolls `1` for learning

- Xairan: We can get more fish in the next dangerous place. Please hide in our pockets
    - Dragon: 🐟 🌑
    
## Traveling to camp

> The party goes on the road.
> They see some berries along the way

- Xairan: Look! Berries!

> The berry seems to be **safe to eat**

- Xairan: Dragon, catch!
    - Dragon [[Checks#Dexterity]] 14

> Dragon catches the berry

- Dragon: 😀
- Xairan: I will keep throwing you berries then all along the journey
    - Dragon rolls a percent die

> The dragon catches 58% of the berries that are thrown. A couple of the missed ones fall on Amrynn's head

- Amrynn: I will forgive you because it is for the good of the little one. I will mumble in elvish when you fail
    - Dragon: 😀
  
> The party approaches the grey hill

- A: How many in this village would be friendly to [[Dredor]]?
    - X: A good chunk, according to the refugees. They group told us where the advantage would be.
    - A: We can go by the village
- X: I will catch some fish. 

> Xairan catches 2 fish and feeds half of one it to the dragon
- D: 😍 (for everyone) 

## Party goes to the village
> The village they arrive before the [[Grey Hill]] has about 15 houses. It has some farmers and people trading
- Amryn: It seems that the village are only talking about normal things
    - X: Maybe find a place to stay
 
 
 ### Inn
 > The party enters a large inn at the end of the village. It seems to be mostly used by locals. There seems to be a wanted poster at the Inn. It seems to be a local comunity hang-out place

- R: Who is this person on the wanted poster?
    - R: [[Checks#Perception]] 23
    - R: There's a picture of a half-orc it has the name of Dresden. It is **not someone they recognize**. It seems to be hand written with a reward of 5GP
- A: I will ring the bell out the counter!

> Someone at the counter comes in, who looks **just like the person on the wanted poster**

- Person: We have 2 rooms. Do you want? It'll be 1SP per night per room
    - R: I will pay!
- A: Can you bring pets?
    - Person: I hope you don't mean horses! We have an arrangement with a local farmer.

> One of the rooms has a **direct view of where they want to go**

- X: I will put on my hat of disguise!
    - X: Roll `1d6`: 3. Transforming into the person from the counter

> The person from the bar 😉 at Xairan, stating that he's seen the trick before.

> The party is now splitting the rooms.

- N: I want a room for myself. And pays for an additional room
- X and A: The room has a bunk bed and perpendicular with a window seeing the [[Grey Hill]]
    - X: It's a bed with a ladder!!
    - A: I will get the top!
    - X: 😓. I've never slept on it!
    - A: Fine. You have it
    - X: [[Checks#Dexterity]] 5 and falls from the ladder
        - The dragon is not hurt,
        - Dragon: 🌑 (to everyone)
- U and R: There are two individual beds!

## Sneak into the camp
> The [[Amryn]] and [[Rircin]] wants to look into the hill. They will sneak into the outskirts of the camp. It looks to be a **boot camp**

- R: Let's see how experienced they are!
    - R: [[Checks#Perception]] 8
    - R: They seem to be decent 
- A: I will try to sneak around and go far
    - R: If I see something I will make a **native bird sound** 🦜
    - A: If I'm not back here in 1 hour I will probably be in trouble. If you see me, then I'll be in trouble.

> Amrynn [[Checks#Stealth]] 11 and sneaks around the camp

> Upon closer inspection, a drill sargent seems to be bluntly teaching the recruits about how to grapple

> Rircin notices that an animal creature **marks similar to an [[Owlbear]] are near**. 

- R: They seem to be **very amateur**
- A: We can release the horses and **make some chaos**
    - A: It seems that they think of themselves as safe. If the camp is handeld by the Son of [[Lord Samuel]], then he thinks that no-one will attack.
-     A: It seems that they think of themselves as safe. If the camp is handled by the Son of [[LordLord Samuel]], then he thinks that no-one will attack

## Tavern
> In the village, [[Norman]], [[Urist]] and [[Xairan]] are drinking beer surrounded by a lot of laughing 

- X: I will start performing music with my flute!
    - X: [[Checks#Performance]] 17.

> One person seems anoyed at the music, but throws a copper piece at Xairan. Everyone else seems to be enjoying it

- Patron: That camp is cursed. They will leave soon enough
    - X: What do you mean?
    - P: There's many leyends. They don't listen to us.
    - X: [[Checks#Insight]] 10

> The patron doesn't seem to be if he's responsible for the curse

-  X: What if an anonymous adventurer came hypothetically to do some damage to the supply lines of the camp? It would help the local farmers!
    -  P: If they find this out, I would face their swords
    -  X: You could tell them that this person is responsible!

> Xairan uses the hat of disguise to dress up as the [[Knight of Dredor]] 

- P: I like your tricks, but I need more actions over words
- X: Looks like we have some fun to do with the bridge, Urist!

> Xairan thanks the barman and proceeds to get Urist and Norman to the suspected bridge.

## Bridge attack

> It seems that small animals and maybe some large ones near the bridge. 

- Dragon: 🌞!!
    - X: I can't! We're stealthing
    - D: 😢🌑

> The river is actually narrower than they expected. It is quite a fast river, so don't fall in.

- X: I want to light up the bridge on fire! What do you think Urist?
    - U: Looks doable
    - X: Pick a dagger! 
    - U: Give me a hack-saw!
    - N: I only have cleric stuff. Many wooden stakes to kill vampires.
- X: Look, you make a distraction and I light up the fire!
    - U: Fine!
- X: I have acids that we could do something with it
    - X: [[Checks#Intelligence]] 1: Doesn't seem to know how to combine acids
    - X [[Checks#Wisdom]] 11: Urist suggestion could be good enough
    
    > Urist, Norman and Xairan are almost near the party

> Urist looks for a nice tree to throw acid to.

- Urist throws some acid to the trunk of a tree to weaken it
    - Urist Attacks with a pickaxe with a natural 1 but fails to topple the tree

> There is noone looking over the bridge

- Urist throws more acid to the

> A sword sticks throughthe hole

The person who is following you might have been **catching you**

- X: I will turn into the night of dredor again with the Hat of Disguise
- R and A: [[Checks#Perception]] 4  and 16 for people manning the bridge
    - They don't see anyone at the bridge
- X: Will throw [[Fireball]] to both ends of the bridge

> The ends of the bridge are now slowly being consumed by the fire into the middle

- A: I see someone running away from the palacide wall!
    - R: I don't think we can't see who it is! They only see one short person running ahead a longer person (It's Urist, but they don't know)

> The bridge keeps burning to the point in which it falls into the river and **is destroyed**

- X: Run Stealthily

> Urist is running away with someone behind!

- U: I will run through the hills.
    - Enemy: It is not so skilled at running on slopes

> Other people in the camp are now looking over at the people running. There's now someone in a horse tracking Urist and the Enemy

- U: I will look for some short terrains that the horse

> The people following after Urist seem to get tired. 
> Amrynn and Rircin are approaching stealthily to see who is being chased.
> They have realised that Urist is the one being chased

- A: The horse looks like it won't go very far. We should follow parallely to the horse rider and the humans following Urist

> Urist is loosing his advantage, but notices that Riricin is following him (he doesn't notice Amrynn)
> The rider seems to have **been thrown** by the horse. He is not having any of it. The rider is now laying on the floor in pain.
> There is no enemy tailing Urist anymore.

> Urist has reached the road and will try to hide 

- U: [[Checks#Stealth]] 15
    - A: [[Message]] to Urist: should we just let you hide?
        - U: Yes, just let me hide
    - A [[Message]] R: Let's just go back to the village to avoid any suspicions from the camp
        - U: Yes , just let me hide
-     A [[Message]] R: Let's just go back to the village to avoid any suspicions from the camp; we can pretend to be travelers who are going for dinner

> Norman stealthily sneaks on Amrynn. She reacts by stabbing him while being startled for 8.

> The party returns to the tavern
> Urist eventually returns to the tavern / inn
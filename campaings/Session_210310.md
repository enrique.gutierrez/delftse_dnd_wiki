# Conversation with Cecilia
## Economy
- Ideas: Give them more loot

## Roles of DM


## Upcoming Story

- Oba plot thread
    - Catlyn mess
        - The half-elf of the quarterlings
        - Working under angarad
        - Who is working against 
- Emlanor thread
    - Get him to hand himself-in


## Stories

- Oba betrays everyone
    - She wants to get her husband back
        - Offered by the governors guard    
            - Waiting for husband
    - She wants to get away
        - She could go North or to Dragonaut

- Emlanor works for dredor
    - Dredor is a faction for Evilan
    - They are a mafia for unahuen
    - Some gov guards are involved with dredor, but not the 
        - Dredor **want to kill Emlanor**

- Catlyn
    - They're going to talk to everyone
    - Catlyn (Vosha's assistant)
        - She knows oba
            - She can do something with the prisoner
        - She told them about the meeting with Sir Alastair
        - She's currently the biggest source of information
    - They would take the prisoner they got 

- Prisoner (scapegoted to be in prison)
    - Norman talked to him, and got him out of here
    - The guy said that he was going to tell on them
    - Was in there for kidnapping
        - Amryn suspects that he was who suspected Elidee's mom
    - The hope is that they take him to Alister and kills him
        - Allister will send them to the Grey Hills

- Someone
    - The information contains Elidee's moms info
        - Sir Evan and Sir Allastair got him guilty
        - The party thinks it was Emlanor

- Grey Hill
    - Viridian is spreading info to get him to be seen as the one true king
    - There is a training camp
        - This is the largest training camp


- Combat encounter (Sentient plants)
    - An overgrown area which is hard to traverse
    - The plants should fight back
        - Solvable via swords
        - Solvable via words DC 

- Encounter with young travelers
    - They don't recommend going to the camp

- At the Inn alternative to the travelers


## Next session

- Agree on Emlenaor
- Go to Catlyin
    - 3 escape attempts
- Gets to Allister
    - "Good that you got him, but **you're wasting our time**"

- Inn on the way to the hill
    - People at the Inn might be talking about creatures that are ruining the camp
    - The ruins that are in the hill people think that it might be cursed
    - The creatures could be a family of "ewoks"
        - Urist could be a 
- Grey hill (camp)
    - Lord Samuel kingdom is on the way to the Grey Hills
        - This is related to Rircin
    - There is a camp lead by Charles, who has a past with [Rircin](../characters/Rircin.md)

 
## Roles


- Next Session
    - Enrique is the prisoner
- Following session
    - 
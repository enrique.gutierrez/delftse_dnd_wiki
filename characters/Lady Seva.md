A very famous knight. 
She's very old now.
She advices the king for certain matters.

When she's not in the city of [Unahuen](../locations/Unahuen.md) she lives in a castle to the south of the [Fox and Goose](../locations/Fox%20and%20Goose.md)

She is older than the knight that trained [Rircin](../characters/Rircin.md)
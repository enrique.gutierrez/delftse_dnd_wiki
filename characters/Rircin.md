# Rircin

- Played: Henry

## Horse
- Thinks that Rircin is the nicest owner he's had.

## Friends

- Anneas is a childhood friend of Rircin
    - At 16 he was a squire with him
        - Anneas was to Samuel
        - Charles was assigned to Rircin
    - Charles demanded and got the village plundered
        - Rircin was conflicted for this
            - Anneas encourages to tell Samuel
    - Samuel listens to Rircin
        - Samuel blames Rircin
        - Anneas let's him escape
- 
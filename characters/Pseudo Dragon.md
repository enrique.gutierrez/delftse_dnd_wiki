# Xiaran's pseudodragon

Tiny dragon, neutral good

Armor Class16(natural armor)
Hit Points7(2d4+2)
Speed15 ft., fly 60 ft.

STR
6(-2)
DEX
15(+2)
CON
10(+0)
INT
11(+0)
WIS
12(+1)
CHA
11(+0)

SkillsStealth +7, Perception +6
SensesBlindsight 10 ft., Darkvision 60 ft., Passive Perception 16
Languages Understands Common and Draconic but can't speak them
Challenge1/4 (50 XP)

Keen Senses. The pseudodragon has advantage on Wisdom (Perception) checks that rely on sight, hearing, or smell.

Magic Resistance. The pseudodragon has advantage on saving throws against spells and other magical effects.

Limited Telepathy. The pseudodragon can magically communicate simple ideas, emotions, and images telepathically with any creature within 100 feet of it that can understand a language.

Familiar. The pseudodragon can serve another creature as a familiar, forming a magic, telepathic bond with that willing companion. While the two are bonded, the companion can sense what the pseudodragon senses as long as they are within 1 mile of each other. While the pseudodragon is within 10 feet of its companion, the companion shares the pseudodragon’s Magic Resistance trait. At any time and for any reason, the pseudodragon can end its service as a familiar, ending the telepathic bond.

Ranger's Companion. The beast obeys your commands as best as it can. It takes its turn on your initiative. On your turn, you can verbally command the beast where to move (no action required by you). You can use your action to verbally command it to take the Attack, Dash, Disengage, or Help action. If you don't issue a command, the beast takes the Dodge action. Once you have the Extra Attack feature, you can make one weapon attack yourself when you command the beast to take the Attack action. If you are incapacitated or absent, the beast acts on its own, focusing on protecting you and itself. The beast never requires your command to use its reaction, such as when making an opportunity attack.

Actions
Beast Master Bonus. The companion gains an additional bonus to attack and damage rolls equal to the ranger's proficiency bonus (not included in the attack descriptions below).

Bite. Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

Shock. Melee Spell Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d8) lightning damage.